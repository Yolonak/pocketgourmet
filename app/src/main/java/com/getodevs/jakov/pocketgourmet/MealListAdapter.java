package com.getodevs.jakov.pocketgourmet;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.os.Environment;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class MealListAdapter extends ArrayAdapter<Meal> {
    private static final String TAG = "MealListAdapter";
    private Context mContext;
    int mResource;
    private int lastPosition = -1;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    final long ONE_MEGABYTE = 1024 * 1024;
    public LruCache<String, Bitmap> memoryCache;

    public MealListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Meal> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
        storage = FirebaseStorage.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();
    }
    private static class ViewHolder {
        TextView tvMealName;
        TextView tvAdress;
        TextView tvPrice;
        ImageView ivImage;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //setupImageLoader();

        String Name = getItem(position).getMealName();
        Bitmap Image = null;
        final String Path = getItem(position).getPath();
        Log.d ("IMAGE PATH;",Path);
        String price=getItem(position).getusdPrice().toString();
        String Adress = getItem(position).getAddress();


        //cache
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        memoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };





        Log.d(TAG, "getView:begin " + Name + "-" + Path + "-" + Image);

        final View result;

        final ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource, parent, false);
            holder= new ViewHolder();

            holder.tvPrice=  convertView.findViewById(R.id.textView1);
            holder.tvMealName=  convertView.findViewById(R.id.textView2);
            holder.tvAdress=  convertView.findViewById(R.id.textView3);
            holder.ivImage=convertView.findViewById(R.id.imageView1);

            result = convertView;

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        /*
        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? R.anim.load_down_anim : R.anim.load_up_anim);
        result.startAnimation(animation);*/


        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? R.anim.load_down_anim : R.anim.load_up_anim);
        result.startAnimation(animation);


        lastPosition = position;
        //ImageLoader imageLoader=ImageLoader.getInstance();
        int defaultImage=mContext.getResources().getIdentifier("@drawable/defpic",null,mContext.getPackageName());
        /*DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(defaultImage)
                .showImageOnFail(defaultImage)
                .showImageOnLoading(defaultImage).build();*/

        holder.tvPrice.setText(price);
        holder.tvMealName.setText(Name);
        holder.tvAdress.setText(Adress);
        holder.ivImage.setImageResource(0);
        /*if (Image!=null) {
            Log.d(TAG, "getView: path" + Path);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Image.compress(Bitmap.CompressFormat.JPEG,25,stream);
            byte[] byteArray = stream.toByteArray();
            Image = BitmapFactory.decodeByteArray(byteArray,0,byteArray.length);
            holder.ivImage=convertView.findViewById(R.id.imageView1);
            holder.ivImage.setImageBitmap(Image);
            holder.ivImage.setVisibility(View.VISIBLE);
        }*/

        //holder.ivImage=convertView.findViewById(R.id.imageView1);
        final String imageKey = String.valueOf(Path);
        final Bitmap bitmap = MyCache.getInstance().retrieveBitmapFromCache(imageKey);
        if (bitmap!=null){
            holder.ivImage.setImageBitmap(bitmap);
            holder.ivImage.setVisibility(View.VISIBLE);
        }
        else{
            final String imageName = Path.split("/")[1];
            Log.d("PATH TO LOOK:",Environment.getExternalStorageDirectory().getAbsolutePath() + "/pocketGourmetImages" + File.separator + imageName);
            Bitmap bitmap1 = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getAbsolutePath() + "/pocketGourmetImages" + File.separator + imageName);
            //Log.d("BITMAP1:",bitmap1.toString());
            if (bitmap1 != null){
                Log.d("LOADING FROM EXTERNAL:",imageName);
                holder.ivImage.setImageBitmap(bitmap1);
                holder.ivImage.setVisibility(View.VISIBLE);
            }
            else{
                Log.d("IMAGE NAME:",imageName);
                storageRef.child(Path).getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                    @Override
                    public void onSuccess(byte[] bytes) {
                        Bitmap Image1 = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                        holder.ivImage.setImageBitmap(Image1);
                        holder.ivImage.setVisibility(View.VISIBLE);
                        MyCache.getInstance().saveBitmapToCahche(String.valueOf(Path),Image1);
                        SaveImage(Image1,imageName);


                    }
                });

            }

        }
        /*
        storageRef.child(Path).getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap Image1 = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                holder.ivImage.setImageBitmap(Image1);
                holder.ivImage.setVisibility(View.VISIBLE);


            }
        });*/

        /*if (!Path.isEmpty()){
            holder.ivImage=convertView.findViewById(R.id.imageView1);
            Log.d("Image", Path);
            Glide.with(getContext())
                    .load(storageRef.child(Path))
                    .into(holder.ivImage);
            holder.ivImage.setVisibility(View.VISIBLE);
        }*/


        return convertView;


    }


    //local storage

    private static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState);
    }

    private static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(extStorageState);
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), albumName);
        if (!file.mkdirs()) {
            Log.e("NOT CREATED", "Directory not created");
        }
        return file;
    }


    private void SaveImage(Bitmap finalBitmap, String iName) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/pocketGourmetImages");
        if (!myDir.mkdirs()) {
            Log.d("PUBLICALBUMDIR:", "Directory not created");
        }
        File file = new File (myDir, iName);
        if (file.exists ()){
            Log.d("EXISTS:","exists");
            file.delete ();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            Log.d("SAVING IMAGE:",file.getAbsolutePath());
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }





/*
    private void setupImageLoader(){

        // UNIVERSAL IMAGE LOADER SETUP
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                mContext)
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);
        // END - UNIVERSAL IMAGE LOADER SETUP
    }*/

}
