package com.getodevs.jakov.pocketgourmet;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class RatingListAdapter extends ArrayAdapter<Rating> {
    private Context mContext;
    int mResource;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private static final String TAG = "RATING LIST ADAPTER";
    private int lastPosition = -1;



    public RatingListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Rating> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
        storage = FirebaseStorage.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();
    }
    private static class ViewHolder {
        TextView tvRating;
        TextView tvComment;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //setupImageLoader();

        String Rate = getItem(position).getRateValue();
        String Comment = getItem(position).getComment();






        Log.d(TAG, "getView:begin " + Rate + "-" + Comment + "-");

        final View result;

        final RatingListAdapter.ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource, parent, false);
            holder= new RatingListAdapter.ViewHolder();

            holder.tvRating=  convertView.findViewById(R.id.rateTextView);
            holder.tvComment=  convertView.findViewById(R.id.commentTextView);

            result = convertView;

            convertView.setTag(holder);
        }
        else {
            holder = (RatingListAdapter.ViewHolder) convertView.getTag();
            result = convertView;
        }



        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? R.anim.load_down_anim : R.anim.load_up_anim);
        result.startAnimation(animation);


        lastPosition = position;

        holder.tvRating.setText(Rate);
        holder.tvComment.setText(Comment);


        return convertView;


    }

}
