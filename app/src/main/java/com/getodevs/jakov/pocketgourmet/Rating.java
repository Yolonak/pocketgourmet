package com.getodevs.jakov.pocketgourmet;

public class Rating {
    private String email;
    private String mealID;
    private String rateValue;
    private String comment;


    public Rating(String email, String mealID, String rateValue, String comment) {
        this.email = email;
        this.mealID = mealID;
        this.rateValue = rateValue;
        this.comment = comment;
    }

    public Rating(){

    }

    public String getEmail() {
        return email;
    }

    public String getMealID() {
        return mealID;
    }

    public String getRateValue() {
        return rateValue;
    }

    public String getComment() {
        return comment;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMealID(String mealID) {
        this.mealID = mealID;
    }

    public void setRateValue(String rateValue) {
        this.rateValue = rateValue;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
