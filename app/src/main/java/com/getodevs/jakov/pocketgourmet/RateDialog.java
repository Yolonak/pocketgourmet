package com.getodevs.jakov.pocketgourmet;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

//import static androidx.constraintlayout.Constraints.TAG;

public class RateDialog extends AppCompatDialogFragment {
    private String layout = "";
    private SeekBar rateSeekBar;
    private EditText commentEditText;
    private String email,mealID,rate,comment;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    CollectionReference mealRef=db.collection("meals");

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.layout_rate_dialog,null);



        builder.setView(view).setTitle("Review").setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton(layout != null ? "OK" : "Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (TextUtils.isEmpty(commentEditText.getText())){
                    comment = "No comment";
                }
                else{
                    comment = commentEditText.getText().toString().trim();
                }
                rate= String.valueOf(rateSeekBar.getProgress());
                Log.d("ADDING RATING",rate + comment);
                if (user.getEmail()!=""){
                    email = user.getEmail();
                    Log.d("ADDING RATING",email);
                }
                else{
                    Log.d("ADDING RATING","user error");
                    Log.d("USER",user.toString());
                }
                if (getArguments()!=null){
                    mealID = getArguments().getString("mealID");
                    Log.d("ADDING RATING","mealID:"+mealID);
                    Rating rateToSubmit = new Rating(email,mealID,rate,comment);
                    Log.d("ADDING RATING:",rateToSubmit.toString());
                }
                final Rating rating= new Rating(email,mealID,rate,comment);
                String name = mealID.split(";;")[0];
                Double latitude = Double.parseDouble(mealID.split(";;")[1]);
                Double longitude = Double.parseDouble(mealID.split(";;")[2]);
                Double price = Double.parseDouble(mealID.split(";;")[3]);
                AddReview(rating,name,latitude,longitude,price);



            }
        });

        rateSeekBar = view.findViewById(R.id.rateSeekBar);
        commentEditText = view.findViewById(R.id.commentEditText);







        return builder.create();
    }

    public void AddReview(final Rating rating, String name, Double latitude, Double longitude, Double price){
        Log.d("AddingReview","meal is good to go");
        db.collection("ratings")
                .add(rating)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("AddingReview","DocumentSnapshot added with ID:" + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("AddingReview", "Error adding document", e);
                    }
                });

        mealRef.whereEqualTo("mealName",name).whereEqualTo("latitude",latitude).whereEqualTo("longitude",longitude).whereEqualTo("usdPrice",price).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("UPDATE DOCUMENT", document.getId() + " => " + document.getData());
                                Log.d("GETTING DATA:","GETTING DATA");
                                Meal singlemeal=document.toObject(Meal.class);
                                db.collection("meals").document(document.getId()).update("mealRatingTotal",singlemeal.getMealRatingTotal()+Double.parseDouble(rating.getRateValue()))
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                Log.d("UPD MRT:","mealRatingTotal updated");
                                            }
                                        });
                                db.collection("meals").document(document.getId()).update("numberOfReviews",singlemeal.getNumberOfReviews()+1)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                Log.d("UPD NOR:","number of reviews updated");
                                            }
                                        });
                            }
                        } else {
                            Log.d("ERROR GETTING DOCUMENTS", "Error getting documents: ", task.getException());
                        }
                    }
                });

    }



/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            listener=(SearchDialog.SearchDialogListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString() + "mustImplementDialogListener");
        }

    }*/
/*
    public interface rateDialogListener{
        void applyText(String minPrice, String maxPrice, String sweet, String sour,String salt, String bitter, String spicy, String fullness);
    }*/
}
