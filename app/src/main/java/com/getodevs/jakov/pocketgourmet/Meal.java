package com.getodevs.jakov.pocketgourmet;

import android.util.Log;

import androidx.annotation.NonNull;

public class Meal {
    public String mealName;
    public Double usdPrice;
    public String currency;
    public Integer timesVerified;
    public String createdAt;
    public String createdBy;
    public Double mealRatingTotal;
    public Integer numberOfReviews;
    public Double latitude;
    public Double longitude;
    //public Double longitudeArea;
    public String address;
    //public String locationID;
    //public String ownedBy;
    public String workDayHours;
    public String saturdayHours;
    public String sundayHours;
    public Double sweetTotal;
    public Integer sweetNumber;
    public Boolean sweetChangeable;
    public Double sourTotal;
    public Integer sourNumber;
    public Boolean sourChangeable;
    public Double saltTotal;
    public Integer saltNumber;
    public Boolean saltChangeable;
    public Double bitterTotal;
    public Integer bitterNumber;
    public Double spicyTotal;
    public Integer spicyNumber;
    public Boolean spicyChangeable;
    public Double umamiTotal;
    public Integer umamiNumber;
    public Double fullnessTotal;
    public Integer fullnessNumber;
    public String mealType;
    public String path;
    public String geoHash;
    private Double localRelevance;
    private Double counter;
    private String primaryTaste;
    private String secondPrimary;
    private String mealSubType;

    public Meal(String mealName, Double usdPrice, String currency, String createdAt, String createdBy, Double latitude, Double longitude, /*Double longitudeArea, */String address,String geoHash, String workDayHours, String saturdayHours, String sundayHours, Double sweetTotal, Boolean sweetChangeable, Double sourTotal, Boolean sourChangeable, Double saltTotal, Boolean saltChangeable, Double bitterTotal, Double spicyTotal,Boolean spicyChangeable,Double umamiTotal, Double fullnessTotal,String mealType, String path, String mealSubType) {
        this.mealName = mealName;
        this.usdPrice = usdPrice;
        this.currency = currency;
        this.timesVerified = 1;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.mealRatingTotal = 0.0;
        this.numberOfReviews = 0;
        this.latitude = latitude;
        this.longitude = longitude;
        //this.longitudeArea=longitudeArea;
        this.address=address;
        this.geoHash= geoHash;
        this.workDayHours = workDayHours;
        this.saturdayHours = saturdayHours;
        this.sundayHours = sundayHours;
        this.sweetTotal = sweetTotal;
        this.sweetNumber = 1;
        this.sweetChangeable = sweetChangeable;
        this.sourTotal = sourTotal;
        this.sourNumber = 1;
        this.sourChangeable = sourChangeable;
        this.saltTotal = saltTotal;
        this.saltNumber = 1;
        this.saltChangeable = saltChangeable;
        this.bitterTotal = bitterTotal;
        this.bitterNumber = 1;
        this.spicyTotal = spicyTotal;
        this.spicyNumber = 1;
        this.spicyChangeable = spicyChangeable;
        this.umamiTotal = umamiTotal;
        this.umamiNumber = 1;
        this.fullnessTotal = fullnessTotal;
        this.fullnessNumber = 1;
        this.mealType = mealType;
        this.path=path;
        //this.localRelevance=0.0;
        Double tasteMax = Math.max(sweetTotal,Math.max(saltTotal,Math.max(sourTotal,Math.max(bitterTotal,Math.max(spicyTotal,umamiTotal)))));
        if (sweetTotal.equals(tasteMax)){
            this.primaryTaste = "sweet";
        }
        else if (saltTotal.equals(tasteMax)){
            this.primaryTaste = "salt";
        }
        else if (sourTotal.equals(tasteMax)){
            this.primaryTaste = "sour";
        }
        else if (bitterTotal.equals(tasteMax)){
            this.primaryTaste = "bitter";
        }
        else if (spicyTotal.equals(tasteMax)){
            this.primaryTaste = "spicy";
        }
        else if (umamiTotal.equals(tasteMax)){
            this.primaryTaste = "umami";
        }

        if (primaryTaste!=null){
            if (saltTotal.equals(tasteMax) && !primaryTaste.equals("salt")){
                this.secondPrimary = "salt";
            }
            else if (sourTotal.equals(tasteMax) && !primaryTaste.equals("sour")){
                this.secondPrimary = "sour";
            }
            else if (bitterTotal.equals(tasteMax) && !primaryTaste.equals("bitter")){
                this.secondPrimary = "bitter";
            }
            else if (spicyTotal.equals(tasteMax) && !primaryTaste.equals("spicy")){
                this.secondPrimary = "spicy";
            }
            else if (umamiTotal.equals(tasteMax) && !primaryTaste.equals("umami")){
                this.secondPrimary = "umami";
            }
            else{
                this.secondPrimary = "none";
            }

        }

        this.mealSubType=mealSubType;
        //this.primaryTaste
    }


    /*public Meal(String mealName, String usdPrice, String currency, String timesVerified, String createdAt, String createdBy, String mealRatingTotal, String numberOfReviews, String latitude, String longitude, String address, String workDayHours, String saturdayHours, String sundayHours, String sweetTotal, String sweetNumber, String sweetChangeable, String sourTotal, String sourNumber, String sourChangeable, String saltTotal, String saltNumber, String saltChangeable, String bitterTotal, String bitterNumber, String spicyTotal, String spicyNumber, String spicyChangeable, String fullnessTotal, String fullnessNumber, String mealType, String path, String geoHash) {
        this.mealName = mealName;
        this.usdPrice = Double.parseDouble(usdPrice);
        this.currency = currency;
        this.timesVerified = Integer.parseInt(timesVerified);
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.mealRatingTotal = Double.parseDouble(mealRatingTotal);
        this.numberOfReviews = Integer.parseInt(numberOfReviews);
        this.latitude = Double.valueOf(latitude);
        this.longitude = Double.valueOf(longitude);
        this.address = address;
        this.workDayHours = workDayHours;
        this.saturdayHours = saturdayHours;
        this.sundayHours = sundayHours;
        this.sweetTotal = Double.valueOf(sweetTotal);
        this.sweetNumber = Integer.valueOf(sweetNumber);
        this.sweetChangeable = Boolean.valueOf(sweetChangeable);
        this.sourTotal = Double.valueOf(sourTotal);
        this.sourNumber = Integer.valueOf(sourNumber);
        this.sourChangeable = Boolean.valueOf(sourChangeable);
        this.saltTotal = Double.valueOf(saltTotal);
        this.saltNumber = Integer.valueOf(saltNumber);
        this.saltChangeable = Boolean.valueOf(saltChangeable);
        this.bitterTotal = Double.valueOf(bitterTotal);
        this.bitterNumber = Integer.valueOf(bitterNumber);
        this.spicyTotal = Double.valueOf(spicyTotal);
        this.spicyNumber = Integer.valueOf(spicyNumber);
        this.spicyChangeable = Boolean.valueOf(spicyChangeable);
        this.fullnessTotal = Double.valueOf(fullnessTotal);
        this.fullnessNumber = Integer.valueOf(fullnessNumber);
        this.mealType = mealType;
        this.path = path;
        this.geoHash = geoHash;
    }*/

//constructor for local data (sve je this.xxxxxx)
    public Meal(String mealName, Double usdPrice, String currency, Integer timesVerified, String createdAt, String createdBy, Double mealRatingTotal, Integer numberOfReviews, Double latitude, Double longitude
            , String address, String workDayHours, String saturdayHours, String sundayHours, Double sweetTotal, Integer sweetNumber, String sweetChangeable, Double sourTotal
            , Integer sourNumber, String sourChangeable, Double saltTotal, Integer saltNumber, String saltChangeable, Double bitterTotal, Integer bitterNumber, Double spicyTotal
            , Integer spicyNumber, String spicyChangeable,Double umamiTotal,Integer umamiNumber, Double fullnessTotal, Integer fullnessNumber, String mealType, String path, String geoHash,
                String primaryTaste, String secondPrimary, String mealSubType) {
        this.mealName = mealName;
        this.usdPrice = usdPrice;
        this.currency = currency;
        this.timesVerified = timesVerified;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.mealRatingTotal = mealRatingTotal;
        this.numberOfReviews = numberOfReviews;
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        this.workDayHours = workDayHours;
        this.saturdayHours = saturdayHours;
        this.sundayHours = sundayHours;
        this.sweetTotal = sweetTotal;
        this.sweetNumber = sweetNumber;
        this.sweetChangeable = Boolean.valueOf(sweetChangeable);
        this.sourTotal = sourTotal;
        this.sourNumber = sourNumber;
        this.sourChangeable = Boolean.valueOf(sourChangeable);
        this.saltTotal = saltTotal;
        this.saltNumber = saltNumber;
        this.saltChangeable = Boolean.valueOf(saltChangeable);
        this.bitterTotal = bitterTotal;
        this.bitterNumber = bitterNumber;
        this.spicyTotal = spicyTotal;
        this.spicyNumber = spicyNumber;
        this.spicyChangeable = Boolean.valueOf(spicyChangeable);
        this.umamiTotal = umamiTotal;
        this.umamiNumber = umamiNumber;
        this.fullnessTotal = fullnessTotal;
        this.fullnessNumber = fullnessNumber;
        this.mealType = mealType;
        this.path = path;
        this.geoHash = geoHash;
        this.primaryTaste = primaryTaste;
        this.secondPrimary = secondPrimary;
        this.mealSubType = mealSubType;

        //Log.d ("MEAL PATH:",path);
        //Log.d ("MEAL NAME:",mealName);
        //Log.d ("MEAL NUMBERS:", numberOfReviews.toString() + bitterNumber + spicyNumber + sourNumber + umamiNumber + saltNumber);
    }

    public Meal()
    {}
/*
    public Double getLongitudeArea() {
        return longitudeArea;
    }

    public void setLongitudeArea(Double longitudeArea) {
        this.longitudeArea = longitudeArea;
    }*/


    public void setLocalRelevance(Double sweetRequired, Double sourRequired, Double saltRequired, Double bitterRequired, Double spicyRequired, Double umamiRequired, Double fullnessRequired) {
        if (sweetRequired>0){
            localRelevance += (Math.abs(this.sweetTotal-sweetRequired));
            counter ++;
        }
        if (sourRequired>0){
            localRelevance += (Math.abs(this.sourTotal-sourRequired));
            counter ++;
        }
        if (saltRequired>0){
            localRelevance += (Math.abs(this.saltTotal-saltRequired));
            counter ++;
        }
        if (bitterRequired>0){
            localRelevance += (Math.abs(this.bitterTotal-bitterRequired));
            counter ++;
        }
        if (spicyRequired>0){
            localRelevance += (Math.abs(this.spicyTotal-spicyRequired));
            counter ++;
        }
        if (umamiRequired>0){
            localRelevance += (Math.abs(this.umamiTotal)-umamiRequired);
            counter ++;
        }
        if (fullnessRequired>0){
            localRelevance += (Math.abs(this.fullnessTotal-spicyRequired));
            counter ++;
        }
        if (counter >= 0){
            localRelevance = localRelevance/counter;
        }
        else{
            localRelevance = 10.0;
        }
        if (localRelevance<10.0){
            this.localRelevance = 10.0 - localRelevance;
        }
        else{
            this.localRelevance = 0.0;
        }


    }

    public String getMealSubType() {
        return mealSubType;
    }

    public void setMealSubType(String mealSubType) {
        this.mealSubType = mealSubType;
    }

    public Double getLocalRelevance() {
        return localRelevance;
    }

    public String getPrimaryTaste() {
        return primaryTaste;
    }

    public String getSecondPrimary() {
        return secondPrimary;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public Double getusdPrice() {
        return usdPrice;
    }

    public void setusdPrice(Double usdPrice) {
        this.usdPrice = usdPrice;
    }

    public Integer getTimesVerified() {
        return timesVerified;
    }

    public void setTimesVerified(Integer timesVerified) {
        this.timesVerified = timesVerified;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Double getMealRatingTotal() {
        return mealRatingTotal;
    }

    public void setMealRatingTotal(Double mealRatingTotal) {
        this.mealRatingTotal = mealRatingTotal;
    }

    public Integer getNumberOfReviews() {
        return numberOfReviews;
    }

    public void setNumberOfReviews(Integer numberOfReviews) {
        this.numberOfReviews = numberOfReviews;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getSweetTotal() {
        return sweetTotal;
    }

    public void setSweetTotal(Double sweetTotal) {
        this.sweetTotal = sweetTotal;
    }

    public Integer getSweetNumber() {
        return sweetNumber;
    }

    public void setSweetNumber(Integer sweetNumber) {
        this.sweetNumber = sweetNumber;
    }

    public Boolean getSweetChangeable() {
        return sweetChangeable;
    }

    public void setSweetChangeable(Boolean sweetChangeable) {
        this.sweetChangeable = sweetChangeable;
    }

    public Double getSourTotal() {
        return sourTotal;
    }

    public void setSourTotal(Double sourTotal) {
        this.sourTotal = sourTotal;
    }

    public Integer getSourNumber() {
        return sourNumber;
    }

    public void setSourNumber(Integer sourNumber) {
        this.sourNumber = sourNumber;
    }

    public Boolean getSourChangeable() {
        return sourChangeable;
    }

    public void setSourChangeable(Boolean sourChangeable) {
        this.sourChangeable = sourChangeable;
    }

    public Double getSaltTotal() {
        return saltTotal;
    }

    public void setSaltTotal(Double saltTotal) {
        this.saltTotal = saltTotal;
    }

    public Integer getSaltNumber() {
        return saltNumber;
    }

    public void setSaltNumber(Integer saltNumber) {
        this.saltNumber = saltNumber;
    }

    public Boolean getSaltChangeable() {
        return saltChangeable;
    }

    public void setSaltChangeable(Boolean saltChangeable) {
        this.saltChangeable = saltChangeable;
    }

    public Double getBitterTotal() {
        return bitterTotal;
    }

    public void setBitterTotal(Double bitterTotal) {
        this.bitterTotal = bitterTotal;
    }

    public Integer getBitterNumber() {
        return bitterNumber;
    }

    public void setBitterNumber(Integer bitterNumber) {
        this.bitterNumber = bitterNumber;
    }

    public Double getSpicyTotal() {
        return spicyTotal;
    }

    public void setSpicyTotal(Double spicyTotal) {
        this.spicyTotal = spicyTotal;
    }

    public Integer getSpicyNumber() {
        return spicyNumber;
    }

    public void setSpicyNumber(Integer spicyNumber) {
        this.spicyNumber = spicyNumber;
    }

    public Boolean getSpicyChangeable() {
        return spicyChangeable;
    }

    public void setSpicyChangeable(Boolean spicyChangeable) {
        this.spicyChangeable = spicyChangeable;
    }

    public Double getFullnessTotal() {
        return fullnessTotal;
    }

    public void setFullnessTotal(Double fullnessTotal) {
        this.fullnessTotal = fullnessTotal;
    }

    public Integer getFullnessNumber() {
        return fullnessNumber;
    }

    public void setFullnessNumber(Integer fullnessNumber) {
        this.fullnessNumber = fullnessNumber;
    }

    public String getGeoHash() {
        return geoHash;
    }

    public void setGeoHash(String geoHash) {
        this.geoHash = geoHash;
    }

    public String getWorkDayHours() {
        return workDayHours;
    }

    public void setWorkDayHours(String workDayHours) {
        this.workDayHours = workDayHours;
    }

    public String getSaturdayHours() {
        return saturdayHours;
    }

    public void setSaturdayHours(String saturdayHours) {
        this.saturdayHours = saturdayHours;
    }

    public String getSundayHours() {
        return sundayHours;
    }

    public void setSundayHours(String sundayHours) {
        this.sundayHours = sundayHours;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMealType() {
        return mealType;
    }

    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

    public Double getUmamiTotal() {
        return umamiTotal;
    }

    public void setUmamiTotal(Double umamiTotal) {
        this.umamiTotal = umamiTotal;
    }

    public Integer getUmamiNumber() {
        return umamiNumber;
    }

    public void setUmamiNumber(Integer umamiNumber) {
        this.umamiNumber = umamiNumber;
    }
}


