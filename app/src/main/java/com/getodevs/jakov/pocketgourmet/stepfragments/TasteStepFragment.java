package com.getodevs.jakov.pocketgourmet.stepfragments;


import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;

import com.getodevs.jakov.pocketgourmet.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class TasteStepFragment extends Fragment implements Step {

    private SeekBar sweetTasteSeekBar,sourTasteSeekBar,saltTasteSeekBar,bitterTasteSeekBar,spicyTasteSeekBar,umamiSeekBar,fullnessSeekBar;
    private CheckBox sweetCB,sourCB,saltCB,spicyCB;
    private EditText titleEditText;
    public TasteData tasteData;

    public interface TasteData{
        public void passTasteValues(ArrayList<String> values);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.taste_step_fragment, container, false);

        sweetTasteSeekBar = v.findViewById(R.id.sweetTasteSeekBar);
        sourTasteSeekBar = v.findViewById(R.id.sourTasteSeekBar);
        saltTasteSeekBar = v.findViewById(R.id.saltTasteSeekBar);
        bitterTasteSeekBar = v.findViewById(R.id.bitterTasteSeekBar);
        spicyTasteSeekBar = v.findViewById(R.id.spicyTasteSeekBar);
        fullnessSeekBar = v.findViewById(R.id.fullnessSeekBar);
        umamiSeekBar = v.findViewById(R.id.umamiTasteSeekBar);

        sweetCB=v.findViewById(R.id.sweetCB);
        sourCB=v.findViewById(R.id.sourCB);
        saltCB=v.findViewById(R.id.saltCB);
        spicyCB=v.findViewById(R.id.spicyCB);

        titleEditText = v.findViewById(R.id.titleEditText);

        //initialize your UI

        return v;
    }

    @Override
    public VerificationError verifyStep() {
        Log.d("VERIFY STEP TASTE","verified step");
        ArrayList<String> dataList = new ArrayList<>();
        if (!TextUtils.isEmpty(titleEditText.getText())){
            dataList.add(titleEditText.getText().toString());
        }
        else{
            dataList.add(null);
        }
        dataList.add(String.valueOf(sweetTasteSeekBar.getProgress()));
        dataList.add(String.valueOf(sourTasteSeekBar.getProgress()));
        dataList.add(String.valueOf(saltTasteSeekBar.getProgress()));
        dataList.add(String.valueOf(bitterTasteSeekBar.getProgress()));
        dataList.add(String.valueOf(spicyTasteSeekBar.getProgress()));
        dataList.add(String.valueOf(fullnessSeekBar.getProgress()));
        dataList.add(String.valueOf(sweetCB.isChecked()));
        dataList.add(String.valueOf(sourCB.isChecked()));
        dataList.add(String.valueOf(saltCB.isChecked()));
        dataList.add(String.valueOf(spicyCB.isChecked()));
        dataList.add(String.valueOf(umamiSeekBar.getProgress()));

        tasteData.passTasteValues(dataList);
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        return null;
    }

    @Override
    public void onSelected() {
        //update UI when selected
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        tasteData = (TasteData)context;
    }



}
