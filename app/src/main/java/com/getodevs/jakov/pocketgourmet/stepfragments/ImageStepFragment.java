package com.getodevs.jakov.pocketgourmet.stepfragments;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.getodevs.jakov.pocketgourmet.BuildConfig;
import com.getodevs.jakov.pocketgourmet.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class ImageStepFragment extends Fragment implements Step {

    private Button pickFromGallery,pickFromCamera;
    private ImageView imageView;
    private static final int GALLERY_REQUEST_CODE=200;
    private static final int CAMERA_REQUEST_CODE=300;
    private Spinner currencySpinner,mealTypeSpinner,mealSubTypeSpinner;
    private EditText priceEditText;

    public ImageData imageData;
    public PriceData priceData;

    //spinner
    private ArrayAdapter<CharSequence> adapter;

    public interface ImageData{
        public void passImageData(Bitmap image);
    }

    public interface PriceData{
        public void passPriceInfo(ArrayList<String> values);
    }

    private String cameraFilePath;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.image_step_fragment, container, false);

        //initialize your UI
        pickFromCamera = v.findViewById(R.id.pickFromCamera);
        pickFromGallery = v.findViewById(R.id.pickFromGallery);
        imageView = v.findViewById(R.id.imageView);

        currencySpinner=v.findViewById(R.id.currencySpinner);
        mealTypeSpinner=v.findViewById(R.id.mealTypeSpinner);
        mealSubTypeSpinner=v.findViewById(R.id.mealSubTypeSpinner);
        priceEditText=v.findViewById(R.id.priceEditText);

        setOnClickListeners();
        setSpinnerListener();

        return v;
    }

    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        if (imageView.getDrawable()!=null){
            imageView.setDrawingCacheEnabled(true);
            imageView.buildDrawingCache();
            Bitmap bitmap=imageView.getDrawingCache().copy(Bitmap.Config.RGB_565, false);
            imageView.destroyDrawingCache();
            //imageView.setDrawingCacheEnabled(false);
            imageData.passImageData(bitmap);
        }
        else{
            imageData.passImageData(null);
        }
        ArrayList<String> dataList = new ArrayList<>();
        if (!TextUtils.isEmpty(priceEditText.getText())){
            dataList.add(priceEditText.getText().toString());
        }
        else{
            dataList.add(null);
        }
        dataList.add(currencySpinner.getSelectedItem().toString());
        dataList.add(mealTypeSpinner.getSelectedItem().toString());
        dataList.add(mealSubTypeSpinner.getSelectedItem().toString());
        priceData.passPriceInfo(dataList);

        return null;
    }

    @Override
    public void onSelected() {
        //update UI when selected
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        imageData = (ImageData) context;
        priceData = (PriceData) context;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==GALLERY_REQUEST_CODE){
            if(resultCode==RESULT_OK){
                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                // Get the cursor
                Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
                //Get the column index of MediaStore.Images.Media.DATA
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                //Gets the String value in the column
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();
                // Set the Image in ImageView after decoding the String
                imageView.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                Log.d("onActivityResult","calledOnActivityResultRESULTOK");
            }
            Log.d("onActivityResult","calledOnActivityResult");
        }
        else if(requestCode==CAMERA_REQUEST_CODE){
            if(resultCode==RESULT_OK){
                imageView.setImageURI(Uri.parse(cameraFilePath));
                Log.d("onActivityResult","calledOnActivityResultCAMERA");
            }
        }
    }

    private void pickFromGallery(){
        //Create an Intent with action as ACTION_PICK
        Intent intent=new Intent(Intent.ACTION_PICK);
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.setType("image/*");
        //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
        // Launching the Intent
        startActivityForResult(intent,GALLERY_REQUEST_CODE);
    }

    private void captureFromCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".provider", createImageFile()));
            startActivityForResult(intent, CAMERA_REQUEST_CODE);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //This is the directory in which the file will be created. This is the default location of Camera photos
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");

        //fix for camera bug
        if (!storageDir.exists()) {
            storageDir.mkdirs();
            storageDir.createNewFile();
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for using again
        cameraFilePath = "file://" + image.getAbsolutePath();
        return image;
    }

    protected void setOnClickListeners() {
        verifyPermissions();
        pickFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickFromGallery();
            }
        });

        pickFromCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureFromCamera();

            }
        });
    }

    private void verifyPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permissions = {android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA};

            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                    permissions[0]) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                    permissions[1]) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                    permissions[2]) == PackageManager.PERMISSION_GRANTED) {
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        permissions,
                        1);
            }
        }
    }

    private void setSpinnerListener(){
        mealSubTypeSpinner.setAdapter(adapter);
        mealTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("ITEM SELECTED:", parent.getItemAtPosition(position).toString());
                String type = parent.getItemAtPosition(position).toString();
                switch (type) {
                    case "healthy":
                        Log.d("IN CASE:", "healthy");
                        adapter = ArrayAdapter.createFromResource(getContext(), R.array.healthy, android.R.layout.simple_spinner_item);
                        adapter.notifyDataSetChanged();
                        mealSubTypeSpinner.setAdapter(adapter);
                        return;
                    case "BBQ":
                        Log.d("IN CASE:", "BBQ");
                        adapter = ArrayAdapter.createFromResource(getContext(), R.array.BBQ, android.R.layout.simple_spinner_item);
                        adapter.notifyDataSetChanged();
                        mealSubTypeSpinner.setAdapter(adapter);
                        return;

                }
            }



                //mealSubTypeSpinner.

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}