package com.getodevs.jakov.pocketgourmet.stepfragments;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.nfc.Tag;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.getodevs.jakov.pocketgourmet.R;
import com.getodevs.jakov.pocketgourmet.timeInfoDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
//import com.google.android.gms.location.places.Place;
//import com.google.android.gms.location.places.ui.PlacePicker;
//import com.google.android.gms.maps.model.LatLng;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;
import com.sucho.placepicker.AddressData;
import com.sucho.placepicker.Constants;
import com.sucho.placepicker.PlacePicker;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.os.Looper.getMainLooper;

public class LocationStepFragment extends Fragment implements Step{

    private Button getMyLocationBtn, selectCustomLocationBtn, addTimeInfoBtn;
    private TextView latitudeTextView, longitudeTextView, addressTextView;

    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private int PLACE_PICKER_REQUEST = 1;
    private static final int REQUEST_LOCATION = 1;
    private PlacePicker.IntentBuilder builder;
    private LocationCallback mLocationCallback;
    private String latitude, longitude, address;
    private String latToPass,longToPass,addToPass;
    //private String workdayHours,saturdayHours,sundayHours = "unknown";

    public LocationData locationData;

    public interface LocationData{
        public void passLocationData(ArrayList<String> values);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.location_step_fragment, container, false);

        getMyLocationBtn = v.findViewById(R.id.getMyLocationBtn);
        selectCustomLocationBtn = v.findViewById(R.id.selectCustomLocationBtn);

        latitudeTextView = v.findViewById(R.id.latitudeTextView);
        longitudeTextView = v.findViewById(R.id.longitudeTextView);
        addressTextView = v.findViewById(R.id.addressTextView);

        fusedLocationProviderClient = new FusedLocationProviderClient(getActivity());
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setFastestInterval(10000);
        locationRequest.setInterval(12000);


        addTimeInfoBtn=v.findViewById(R.id.addTimeInfoBtn);

        selectCustomLocationBtn.setEnabled(false);
        requestLocationUpdates();
        setOnClickListeners();




        //initialize your UI

        return v;
    }

    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        Log.d("VERIFY LOCATION STEP","verified step");
        ArrayList<String> dataList = new ArrayList<>();

        dataList.add(latToPass);
        dataList.add(longToPass);
        dataList.add(addToPass);

        locationData.passLocationData(dataList);

        return null;
    }


    @Override
    public void onSelected() {
        //update UI when selected
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }

    private void setOnClickListeners() {
        getMyLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestLocationUpdates("");
                /*
                latitudeTextView.setText("Latitude:" + latitude);
                longitudeTextView.setText("Longitude:" + longitude);
                address = getCompleteAddressString(Double.parseDouble(latitude), Double.parseDouble(longitude));
                addressTextView.setText(address);
                latToPass = latitude;
                longToPass = longitude;
                addToPass = address;*/

            }
        });
        if (isServicesOK()) {
            selectCustomLocationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*    //old place picker
                    try {
                        fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    builder = new PlacePicker.IntentBuilder();
                    try {
                        startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    }*/
                    builder = new PlacePicker.IntentBuilder()
                            .setLatLong(Double.parseDouble(latitude), Double.parseDouble(longitude))  // Initial Latitude and Longitude the Map will load into
                            .showLatLong(true)  // Show Coordinates in the Activity
                            .setMapZoom(12.0f)  // Map Zoom Level. Default: 14.0
                            .setAddressRequired(true) // Set If return only Coordinates if cannot fetch Address for the coordinates. Default: True
                            .hideMarkerShadow(true); // Hides the shadow under the map marker. Default: False
                            /*
                            .setMarkerDrawable(R.drawable.marker) // Change the default Marker Image
                            .setMarkerImageImageColor(R.color.colorPrimary)
                            .setFabColor(R.color.fabColor)
                            .setPrimaryTextColor(R.color.primaryTextColor) // Change text color of Shortened Address
                            .setSecondaryTextColor(R.color.secondaryTextColor) // Change text color of full Address*/
                            //.build(getActivity());

                    startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
                }
            });

        }

        addTimeInfoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimeDialog();
            }
        });
    }

    private void requestLocationUpdates(){
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
        mLocationCallback=new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                latitude=String.valueOf(locationResult.getLastLocation().getLatitude());
                longitude=String.valueOf(locationResult.getLastLocation().getLongitude());
                selectCustomLocationBtn.setEnabled(true);

                /*latitudeTextView.setText("Latitude:" + latitude);
                longitudeTextView.setText("Longitude:" + longitude);
                addressTextView.setText(getCompleteAddressString(Double.parseDouble(latitude),Double.parseDouble(longitude)));*/
                /*try{
                    fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
                }
                catch (Exception e){
                    e.printStackTrace();
                }*/
            }
        };
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)== PermissionChecker.PERMISSION_GRANTED){
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, mLocationCallback, getMainLooper());
        }

    }

    private void requestLocationUpdates(String x){
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
        mLocationCallback=new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                latitude=String.valueOf(locationResult.getLastLocation().getLatitude());
                longitude=String.valueOf(locationResult.getLastLocation().getLongitude());
                selectCustomLocationBtn.setEnabled(true);
                latitudeTextView.setText("Latitude:" + latitude);
                longitudeTextView.setText("Longitude:" + longitude);
                address = getCompleteAddressString(Double.parseDouble(latitude), Double.parseDouble(longitude));
                addressTextView.setText(address);
                latToPass = latitude;
                longToPass = longitude;
                addToPass = address;

                /*latitudeTextView.setText("Latitude:" + latitude);
                longitudeTextView.setText("Longitude:" + longitude);
                addressTextView.setText(getCompleteAddressString(Double.parseDouble(latitude),Double.parseDouble(longitude)));*/
                try{
                    fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)== PermissionChecker.PERMISSION_GRANTED){
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, mLocationCallback, getMainLooper());
        }

    }

    private void openTimeDialog(){
        timeInfoDialog timeInfoDialog = new timeInfoDialog();
        timeInfoDialog.show(getFragmentManager(),"timeInfoDialog");

    }


    /*
    @Override
    public void applyTimeInfo(String WDO, String WDC, String STO, String STC, String SNO, String SNC) {
        Log.d("TimePassed",WDO + " " + WDC + " " + STO +" " + STC + " " + SNO + " " + SNC);
        workdayHours = WDO + "-" + WDC;
        saturdayHours = STO + "-" + STC;
        sundayHours = SNO + "-" + SNC;
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (requestCode == PLACE_PICKER_REQUEST) {
            /*OLD PLACE PICKER
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(getActivity(), data);
                LatLng latLng = place.getLatLng();
                latitude = String.valueOf(latLng.latitude);
                longitude = String.valueOf(latLng.longitude);
                //Toast.makeText(getApplicationContext(), latitude + " " + longitude, Toast.LENGTH_SHORT).show();
                latitudeTextView.setText("Latitude:" + latitude);
                longitudeTextView.setText("Longitude:" + longitude);
                addressTextView.setText(getCompleteAddressString(Double.parseDouble(latitude), Double.parseDouble(longitude)));
            }*/
            if (resultCode == Activity.RESULT_OK && data != null) {
                //LatLng latLng = data.getParcelableExtra(Constants.SHOW_LAT_LONG_INTENT);
                AddressData addressData = data.getParcelableExtra(Constants.ADDRESS_INTENT);
                latitude = String.valueOf(addressData.getLatitude());
                longitude = String.valueOf(addressData.getLongitude());
                address = getCompleteAddressString(Double.parseDouble(latitude), Double.parseDouble(longitude));
                latitudeTextView.setText("Latitude:" + latitude);
                longitudeTextView.setText("Longitude:" + longitude);
                addressTextView.setText(address);
                latToPass = latitude;
                longToPass = longitude;
                addToPass = address;
                //addressTextView.setText(addressData.getAddressList().toArray()[0].toString());

                //Log.d("LATLONG:",addressData.toString());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private boolean isServicesOK(){
        Log.d("SearchActivity", "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getActivity());

        if(available == ConnectionResult.SUCCESS){
            //everything is fine and the user can make map requests
            Log.d("SearchActivity", "isServicesOK: Google Play Services is working");
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available)){
            //an error occured but we can resolve it
            Log.d("SearchActivity", "isServicesOK: an error occured but we can fix it");
            Toast.makeText(getActivity(), "You can't make map requests", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getActivity(), "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {

        String strAdd = "";
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current address", strReturnedAddress.toString());
            } else {
                Log.w("My Current address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current address", "Can't get Address!");
        }
        return strAdd;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        locationData = (LocationData)context;
    }

}