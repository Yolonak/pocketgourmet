package com.getodevs.jakov.pocketgourmet;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Collections;

public class Fragment_map extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "Fragment_map";
    LatLng startLocation;
    String latitude,longitude;
    GoogleMap map;
    int count = 0;
    ArrayList<Marker> markerList;
    SharedPreferences prefs;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;

    @Nullable
    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //prefs = getActivity().getSharedPreferences("MyValues",0);
        return view;


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        latitude=((ViewActivity)getActivity()).getLatitudeFromView();
        longitude=((ViewActivity)getActivity()).getLongitudeFromView();

        Log.d(TAG,latitude + " " + longitude);
        startLocation = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));

    }


    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(final GoogleMap googleMap) {
        map = googleMap;

        googleMap.setMinZoomPreference(8);
        googleMap.setMyLocationEnabled(true);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(startLocation, 12.0f));

        loadMarkers();
    }






    private void loadMarkers() {
        markerList = new ArrayList<>();
        int i = 0;
        for (Meal meal : ((ViewActivity) getActivity()).getListData()) {
            LatLng pos = new LatLng(meal.getLatitude(), meal.getLongitude());
            Marker m = map.addMarker(new MarkerOptions()
                    .position(pos)
                    .anchor(0.5f, 0.5f)
                    .title(meal.getMealName())
                    .icon(BitmapDescriptorFactory.defaultMarker(42))
                    .snippet(meal.getusdPrice()+"   " + meal.getAddress()));
            m.setTag(i++);
            markerList.add(m);
        }
        Collections.reverse(markerList);

    }






}
