package com.getodevs.jakov.pocketgourmet;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

public class timeInfoDialog extends DialogFragment {
    private TextView workDayOpen;
    private TextView workDayClose;
    private TextView saturdayOpen;
    private TextView saturdayClose;
    private TextView sundayOpen;
    private TextView sundayClose;
    private timeInfoDialogListener timeListener;
    private TimePickerDialog timePickerDialog;


    private Button workDayOpenBtn,workDayCloseBtn,saturdayOpenBtn,saturdayCloseBtn,sundayOpenBtn,sundayCloseBtn;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_add_time_info_dialog, null);
        builder.setView(view).setTitle("Select time info").setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String WDO=workDayOpen.getText().toString();
                String WDC=workDayClose.getText().toString();
                String STO=saturdayOpen.getText().toString();
                String STC=saturdayClose.getText().toString();
                String SNO=sundayOpen.getText().toString();
                String SNC=sundayClose.getText().toString();
                timeListener.applyTimeInfo(WDO,WDC,STO,STC,SNO,SNC);

            }
        });
        workDayOpen = view.findViewById(R.id.workDayOpenTimeTextView);
        workDayClose = view.findViewById(R.id.workDayCloseTimeTextView);
        saturdayOpen = view.findViewById(R.id.saturdayOpenTimeTextView);
        saturdayClose = view.findViewById(R.id.saturdayCloseTimeTextView);
        sundayOpen = view.findViewById(R.id.sundayOpenTimeTextView);
        sundayClose = view.findViewById(R.id.sundayCloseTimeTextView);

        workDayOpenBtn = view.findViewById(R.id.openTimeWorkDayBtn);
        workDayCloseBtn = view.findViewById(R.id.closeTimeWorkDayBtn);
        saturdayOpenBtn = view.findViewById(R.id.openTimeSaturdayBtn);
        saturdayCloseBtn = view.findViewById(R.id.closeTimeSaturdayBtn);
        sundayOpenBtn = view.findViewById(R.id.openTimeSundayBtn);
        sundayCloseBtn = view.findViewById(R.id.closeTimeSundayBtn);
        setOnClickListeners();
        return builder.create();
    }


    private void setOnClickListeners(){
        workDayOpenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*TimePickerFragment timePicker = new TimePickerFragment();
                timePicker.show(getActivity().getSupportFragmentManager(),"timePicker");*/
                timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Log.d("TimePicker:", String.valueOf(hourOfDay) + " " + String.valueOf(minute));
                        workDayOpen.setText(String.format("%02d:%02d",hourOfDay,minute));
                    }
                },0,0,true);
                timePickerDialog.show();
            }
        });

        workDayCloseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*TimePickerFragment timePicker = new TimePickerFragment();
                timePicker.show(getActivity().getSupportFragmentManager(),"timePicker");*/
                timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Log.d("TimePicker:", String.valueOf(hourOfDay) + " " + String.valueOf(minute));
                        workDayClose.setText(String.format("%02d:%02d",hourOfDay,minute));
                    }
                },0,0,true);
                timePickerDialog.show();
            }
        });

        saturdayOpenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*TimePickerFragment timePicker = new TimePickerFragment();
                timePicker.show(getActivity().getSupportFragmentManager(),"timePicker");*/
                timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Log.d("TimePicker:", String.valueOf(hourOfDay) + " " + String.valueOf(minute));
                        saturdayOpen.setText(String.format("%02d:%02d",hourOfDay,minute));
                    }
                },0,0,true);
                timePickerDialog.show();
            }
        });

        saturdayCloseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*TimePickerFragment timePicker = new TimePickerFragment();
                timePicker.show(getActivity().getSupportFragmentManager(),"timePicker");*/
                timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Log.d("TimePicker:", String.valueOf(hourOfDay) + " " + String.valueOf(minute));
                        saturdayClose.setText(String.format("%02d:%02d",hourOfDay,minute));
                    }
                },0,0,true);
                timePickerDialog.show();
            }
        });

        sundayOpenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*TimePickerFragment timePicker = new TimePickerFragment();
                timePicker.show(getActivity().getSupportFragmentManager(),"timePicker");*/
                timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Log.d("TimePicker:", String.valueOf(hourOfDay) + " " + String.valueOf(minute));
                        sundayOpen.setText(String.format("%02d:%02d",hourOfDay,minute));
                    }
                },0,0,true);
                timePickerDialog.show();
            }
        });

        sundayCloseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*TimePickerFragment timePicker = new TimePickerFragment();
                timePicker.show(getActivity().getSupportFragmentManager(),"timePicker");*/
                timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Log.d("TimePicker:", String.valueOf(hourOfDay) + " " + String.valueOf(minute));
                        sundayClose.setText(String.format("%02d:%02d",hourOfDay,minute));
                    }
                },0,0,true);
                timePickerDialog.show();
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("ONATACH:",context.toString());
        try{
            timeListener = (timeInfoDialogListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString() + "implementing problem at main activity");
        }
    }

    public interface timeInfoDialogListener{
        void applyTimeInfo(String WDO, String WDC, String STO, String STC, String SNO, String SNC);
    }

}
