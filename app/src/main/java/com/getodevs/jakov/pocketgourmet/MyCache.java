package com.getodevs.jakov.pocketgourmet;

import android.graphics.Bitmap;
import android.util.LruCache;

public class MyCache {

    private static MyCache instance;
    private LruCache<Object, Object> lru;

    private MyCache() {

        lru = new LruCache<Object, Object>(1024);

    }

    public static MyCache getInstance() {

        if (instance == null) {
            instance = new MyCache();
        }
        return instance;

    }

    public LruCache<Object, Object> getLru() {
        return lru;
    }

    public void saveBitmapToCahche(String key, Bitmap bitmap){

        MyCache.getInstance().getLru().put(key, bitmap);
    }

    public Bitmap retrieveBitmapFromCache(String key){

        Bitmap bitmap = (Bitmap)MyCache.getInstance().getLru().get(key);
        return bitmap;
    }

}
