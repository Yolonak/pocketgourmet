package com.getodevs.jakov.pocketgourmet;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ManageAccActivity extends AppCompatActivity {

    private EditText itemToChangeOldEditText,itemToChangeNewEditText;
    private Button changeEmailBtn,changePasswordBtn,deleteAccBtn,logOutBtn;
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_acc);
        //get firebase auth instance
        auth = FirebaseAuth.getInstance();
        //get current user
        user = FirebaseAuth.getInstance().getCurrentUser();
        changeEmailBtn=(Button)findViewById(R.id.changeEmailBtn);
        changePasswordBtn=(Button)findViewById(R.id.changePasswordBtn);
        deleteAccBtn=(Button)findViewById(R.id.deleteAccBtn);
        logOutBtn=(Button)findViewById(R.id.logOutBtn);
        itemToChangeNewEditText=(EditText)findViewById(R.id.itemToChangeNewEditText);
        itemToChangeOldEditText=(EditText)findViewById(R.id.itemToChangeOldEditText);
        setOnClickListeners();
    }


    public void setOnClickListeners(){
        changeEmailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(),itemToChangeOldEditText.getText().toString());
                user.reauthenticate(credential)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    if (user != null && !itemToChangeNewEditText.getText().toString().trim().equals("")) {
                                        if (itemToChangeNewEditText.getText().toString().trim().length() < 6) {
                                            itemToChangeNewEditText.setError("Password too short, enter minimum 6 characters");
                                            itemToChangeNewEditText.setVisibility(View.GONE);
                                        } else {
                                            user.updateEmail(itemToChangeNewEditText.getText().toString().trim())
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                Toast.makeText(ManageAccActivity.this, "Email address is updated.", Toast.LENGTH_LONG).show();
                                                            } else {
                                                                Toast.makeText(ManageAccActivity.this, "Failed to update email!", Toast.LENGTH_LONG).show();
                                                            }
                                                            auth.signOut();
                                                            startActivity(new Intent(ManageAccActivity.this, LogInActivity.class));
                                                        }
                                                    });
                                        }
                                    } else if (itemToChangeNewEditText.getText().toString().trim().equals("")) {
                                        itemToChangeNewEditText.setError("Enter password");
                                    }
                                }
                                else {
                                    Toast.makeText(ManageAccActivity.this, "Old pass incorect", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
        changePasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(),itemToChangeOldEditText.getText().toString());
                user.reauthenticate(credential)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    if (user != null && !itemToChangeNewEditText.getText().toString().trim().equals("")) {
                                        if (itemToChangeNewEditText.getText().toString().trim().length() < 6) {
                                            itemToChangeNewEditText.setError("Password too short, enter minimum 6 characters");
                                            itemToChangeNewEditText.setVisibility(View.GONE);
                                        } else {
                                            user.updatePassword(itemToChangeNewEditText.getText().toString().trim())
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                Toast.makeText(ManageAccActivity.this, "Password is updated, sign in with new password!", Toast.LENGTH_SHORT).show();
                                                                auth.signOut();
                                                                startActivity(new Intent(ManageAccActivity.this, LogInActivity.class));
                                                            } else {
                                                                Toast.makeText(ManageAccActivity.this, "Failed to update password!", Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    });
                                        }
                                    } else if (itemToChangeNewEditText.getText().toString().trim().equals("")) {
                                        itemToChangeNewEditText.setError("Enter password");
                                    }
                                }
                                else {
                                    Toast.makeText(ManageAccActivity.this, "Old pass incorect", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
        deleteAccBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(),itemToChangeOldEditText.getText().toString());
                user.reauthenticate(credential)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(ManageAccActivity.this);
                                    builder1.setMessage("YOU ARE ABOUT TO DELETE YOUR ACCOUNT,ARE YOU SURE");
                                    builder1.setCancelable(true);

                                    builder1.setPositiveButton(
                                            "Yes",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    user.delete()
                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if (task.isSuccessful()) {
                                                                        Log.d("DELETE ACCOUNT:", "User account deleted.");
                                                                        startActivity(new Intent(ManageAccActivity.this, LogInActivity.class));
                                                                    }
                                                                }
                                                            });
                                                }
                                            });

                                    builder1.setNegativeButton(
                                            "No",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });

                                    AlertDialog alert11 = builder1.create();
                                    alert11.show();
                                }
                                else {
                                    Toast.makeText(ManageAccActivity.this, "Old pass incorect", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

            }
        });
        logOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auth.signOut();
                startActivity(new Intent(ManageAccActivity.this, LogInActivity.class));
            }
        });

    }
}



