package com.getodevs.jakov.pocketgourmet;

import androidx.appcompat.app.AppCompatDialogFragment;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;

//import static androidx.constraintlayout.Constraints.TAG;

public class SearchDialog extends AppCompatDialogFragment {

    private static final String TAG = "SEARCHDIALOG";
    private EditText minPriceET,maxPriceET;
    private SeekBar sweetTasteSeekBar,sourTasteSeekBar,saltTasteSeekBar,bitterTasteSeekBar,spicyTasteSeekBar,umamiTasteSeekBar,fullnessSeekBar;
    private Spinner mealTypeSpinner, mealSubTypeSpinner;
    private SearchDialogListener listener;
    //private ArrayList<String> meals;
    private String layout = "";

    //spinner
    private ArrayAdapter<CharSequence> adapter;




    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.layout_search_dialog,null);

        //meals = new ArrayList<>();

        Log.d(TAG, "onCreateDialog:1-" + layout + "-");
        //mealList = (ArrayList<String>) getArguments().getSerializable("meals");
        //layout = getArguments().getString("layout");
        Log.d(TAG, "onCreateDialog:-" + layout + "-");


        builder.setView(view).setTitle("Filter").setNegativeButton("NewSearch", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                /*
                String minPrice=minPriceET.getText().toString();
                String maxPrice= maxPriceET.getText().toString();
                if (minPrice.isEmpty()){
                    minPrice="0";
                }
                if (maxPrice.isEmpty()){
                    maxPrice="9999999";
                }*/

                String sweet=String.valueOf(sweetTasteSeekBar.getProgress());
                String sour=String.valueOf(sourTasteSeekBar.getProgress());
                String salt=String.valueOf(saltTasteSeekBar.getProgress());
                String bitter=String.valueOf(bitterTasteSeekBar.getProgress());
                String spicy=String.valueOf(spicyTasteSeekBar.getProgress());
                String fullness=String.valueOf(fullnessSeekBar.getProgress());
                String umami = String.valueOf(umamiTasteSeekBar.getProgress());
                String mealType = mealTypeSpinner.getSelectedItem().toString();
                String mealSubType;
                if (mealSubTypeSpinner.getSelectedItem()!=null){
                    mealSubType = mealSubTypeSpinner.getSelectedItem().toString();
                }
                else{
                    mealSubType="";
                }
                if (mealSubType.equals("")){
                    mealSubType="unspecified";
                }
                Log.d("SEARCH DIALOG:", "onClick: " + mealType + " " + mealSubType + " " + " " /* + meals.size()*/);
                if (/*!minPrice.equals("0") || !maxPrice.equals("9999999") || */!sweet.equals("0")|| !sour.equals("0")|| !salt.equals("0")|| !bitter.equals("0")
                        || !spicy.equals("0")|| !umami.equals("0") ||!fullness.equals("0")|| !mealType.equals("unspecified")){
                    //listener.reloadDataWithUpdatedParameters(minPrice,maxPrice,sweet, sour, salt, bitter, spicy, umami, fullness);
                    listener.reloadDataWithUpdatedParameters(mealType,mealSubType,sweet, sour, salt, bitter, spicy, umami, fullness);
                }

            }
        }).setPositiveButton(layout != null ? "FilterExisting" : "Search", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                /*
                String minPrice=minPriceET.getText().toString();
                String maxPrice= maxPriceET.getText().toString();
                if (minPrice.isEmpty()){
                    minPrice="0";
                }
                if (maxPrice.isEmpty()){
                    maxPrice="9999999";
                }
                */
                String sweet=String.valueOf(sweetTasteSeekBar.getProgress());
                String sour=String.valueOf(sourTasteSeekBar.getProgress());
                String salt=String.valueOf(saltTasteSeekBar.getProgress());
                String bitter=String.valueOf(bitterTasteSeekBar.getProgress());
                String spicy=String.valueOf(spicyTasteSeekBar.getProgress());
                String fullness=String.valueOf(fullnessSeekBar.getProgress());
                String umami = String.valueOf(umamiTasteSeekBar.getProgress());
                String mealType = mealTypeSpinner.getSelectedItem().toString();
                String mealSubType = mealSubTypeSpinner.getSelectedItem().toString();
                if (mealSubType.equals("")){
                    mealSubType="unspecified";
                }

                /*Integer sweet=sweetTasteSeekBar.getProgress();
                Integer sour=sourTasteSeekBar.getProgress();
                Integer salt=saltTasteSeekBar.getProgress();
                Integer bitter=bitterTasteSeekBar.getProgress();
                Integer spicy=spicyTasteSeekBar.getProgress();
                Integer fullness=fullnessSeekBar.getProgress();*/
                //Log.d(TAG, "onClick: " + minPrice + " " + maxPrice + " " + umami + " " /* + meals.size()*/);
                if (/*!minPrice.equals("0") || !maxPrice.equals("9999999") || */!sweet.equals("0")|| !sour.equals("0")|| !salt.equals("0")|| !bitter.equals("0")
                        || !spicy.equals("0")|| !umami.equals("0") || !fullness.equals("0") || !mealType.equals("unspecified")){
                    //listener.applyText(minPrice,maxPrice,sweet, sour, salt, bitter, spicy, umami, fullness);
                    listener.applyText(mealType,mealSubType,sweet, sour, salt, bitter, spicy, umami, fullness);
                }

            }
        });




        /*
        minPriceET = view.findViewById(R.id.minPriceEditText);
        minPriceET.setVisibility(View.VISIBLE);
        maxPriceET = view.findViewById(R.id.maxPriceEditText);
        maxPriceET.setVisibility(View.VISIBLE);
        */
        sweetTasteSeekBar = view.findViewById(R.id.sweetTasteSeekBar);
        sweetTasteSeekBar.setVisibility(View.VISIBLE);
        sourTasteSeekBar = view.findViewById(R.id.sourTasteSeekBar);
        sourTasteSeekBar.setVisibility(View.VISIBLE);
        saltTasteSeekBar = view.findViewById(R.id.saltTasteSeekBar);
        saltTasteSeekBar.setVisibility(View.VISIBLE);
        bitterTasteSeekBar = view.findViewById(R.id.bitterTasteSeekBar);
        bitterTasteSeekBar.setVisibility(View.VISIBLE);
        spicyTasteSeekBar = view.findViewById(R.id.spicyTasteSeekBar);
        spicyTasteSeekBar.setVisibility(View.VISIBLE);
        umamiTasteSeekBar = view.findViewById(R.id.umamiTasteSeekBar);
        umamiTasteSeekBar.setVisibility(View.VISIBLE);
        fullnessSeekBar = view.findViewById(R.id.fullnessSeekBar);
        fullnessSeekBar.setVisibility(View.VISIBLE);
        mealTypeSpinner = view.findViewById(R.id.mealTypeSpinner);
        mealTypeSpinner.setVisibility(View.VISIBLE);
        mealSubTypeSpinner = view.findViewById(R.id.mealSubTypeSpinner);
        mealSubTypeSpinner.setVisibility(View.VISIBLE);
        setSpinnerListener();

        /*
        boolean booly = layout == null;
        Log.d(TAG, "onCreateDialog:booly " + booly);*/


        return builder.create();
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            listener=(SearchDialogListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString() + "mustImplementDialogListener");
        }

    }

    public interface SearchDialogListener{
        //void applyText(String minPrice, String maxPrice, String sweet, String sour,String salt, String bitter, String spicy, String umami, String fullness);
        void applyText(String mealType, String mealSubType, String sweet, String sour,String salt, String bitter, String spicy, String umami, String fullness);
        //void reloadDataWithUpdatedParameters(String minPrice, String maxPrice, String sweet, String sour,String salt, String bitter, String spicy, String umami, String fullness);
        void reloadDataWithUpdatedParameters(String mealType, String mealSubType, String sweet, String sour,String salt, String bitter, String spicy, String umami, String fullness);
    }

    private void setSpinnerListener(){
        mealSubTypeSpinner.setAdapter(adapter);
        mealTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("ITEM SELECTED:", parent.getItemAtPosition(position).toString());
                String type = parent.getItemAtPosition(position).toString();
                switch (type) {
                    case "healthy":
                        Log.d("IN CASE:", "healthy");
                        adapter = ArrayAdapter.createFromResource(getContext(), R.array.healthy, android.R.layout.simple_spinner_item);
                        adapter.notifyDataSetChanged();
                        mealSubTypeSpinner.setAdapter(adapter);
                        return;
                    case "BBQ":
                        Log.d("IN CASE:", "BBQ");
                        adapter = ArrayAdapter.createFromResource(getContext(), R.array.BBQ, android.R.layout.simple_spinner_item);
                        adapter.notifyDataSetChanged();
                        mealSubTypeSpinner.setAdapter(adapter);
                        return;

                }
            }



            //mealSubTypeSpinner.

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}
