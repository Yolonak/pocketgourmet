package com.getodevs.jakov.pocketgourmet;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.getodevs.jakov.pocketgourmet.localstorage.DatabaseHelper;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.fonfon.geohash.GeoHash;
//import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
//import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
//import com.google.android.gms.location.places.Place;
//import com.google.android.gms.location.places.ui.PlacePicker;
//import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.sucho.placepicker.AddressData;
import com.sucho.placepicker.Constants;
import com.sucho.placepicker.PlacePicker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static java.lang.Math.abs;

public class ViewActivity extends AppCompatActivity implements SearchDialog.SearchDialogListener{
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private String mlatitude, mlongitude;
    private int iteration;
    private static final int REQUEST_LOCATION = 1;
    LocationCallback mLocationCallback;
    Button placeHolder, placeHolderLocation;
    String secondStartHash;
    String firstStopHash;
    String type;
    DatabaseHelper dbHelper;

    //place picker
    int PLACE_PICKER_REQUEST = 1;
    PlacePicker.IntentBuilder builder;

    //primaryTaste
    String primaryTaste=null;
    String secondPrimary=null;

    //strings
    private String minPriceA,maxPriceA,sweetA,sourA,saltA,bitterA,spicyA,umamiA,fullnessA,mealTypeA,mealSubTypeA=null;


    private static final String TAG = "View Activity";
    ArrayList<Meal> databaseData;
    ArrayList<Meal> listData;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    CollectionReference mealRef=db.collection("meals");


    //SHARED PREFS, LIMIT TO READS
    SharedPreferences.Editor editor;
    SharedPreferences prefs;
    Date lastReadDate;
    String formattedDate;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private String LIMIT_TO_READ;
    private int today;

    //private Double sweet,sour,salt,bitter,spicy,fullness;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //limit
        lastReadDate = Calendar.getInstance().getTime();
        LIMIT_TO_READ = user.toString();
        SimpleDateFormat df = new SimpleDateFormat("ddMMMyyyy");
        formattedDate = df.format(lastReadDate);
        Log.d("DATE:",formattedDate);



        iteration = 0;
        secondStartHash= "";
        firstStopHash= "";
        fusedLocationProviderClient = new FusedLocationProviderClient(this);
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setFastestInterval(18000);
        locationRequest.setInterval(40000);
        requestLocationUpdates();
        setContentView(R.layout.activity_view);
        //mlocation part
        type = getIntent().getStringExtra("type");
        Log.d("TYPE:",type);
        dbHelper = new DatabaseHelper(this);

        placeHolder = findViewById(R.id.placeHolder);
        placeHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("VIEW ACTIVITY:","clicked");
                openDialog();
            }
        });

        placeHolderLocation = findViewById(R.id.placeHolderLocation);
        placeHolderLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder = new PlacePicker.IntentBuilder()
                        .setLatLong(Double.parseDouble(mlatitude), Double.parseDouble(mlongitude))  // Initial Latitude and Longitude the Map will load into
                        .showLatLong(true)  // Show Coordinates in the Activity
                        .setMapZoom(12.0f)  // Map Zoom Level. Default: 14.0
                        .setAddressRequired(true) // Set If return only Coordinates if cannot fetch Address for the coordinates. Default: True
                        .hideMarkerShadow(true); // Hides the shadow under the map marker. Default: False

                startActivityForResult(builder.build(ViewActivity.this), PLACE_PICKER_REQUEST);
            }
        });



        listData= new ArrayList<Meal>();
        databaseData = new ArrayList<Meal>();

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Fragment_list()).commit();
    }

    public ArrayList<Meal> getListData() {

        return listData;
    }

    public String getLatitudeFromView(){
        return mlatitude;
    }
    public String getLongitudeFromView(){
        return mlongitude;
    }

    private void query_ps(String startHash, String endHash, String primaryTaste, String secondPrimary){
        mealRef.whereLessThan("geoHash",endHash).whereGreaterThan("geoHash",startHash).whereEqualTo("primaryTaste",primaryTaste).whereEqualTo("secondPrimary",secondPrimary).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            int number=0;

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Log.d("GETTING DATA:","GETTING DATA");
                                Meal singlemeal=document.toObject(Meal.class);
                                listData.add(singlemeal);
                                databaseData.add(singlemeal);
                                //singlemeal.setLocalRelevance();
                                number += 1;
                                dbHelper.addData(singlemeal);
                            }
                            //shared prefs
                            editor = getSharedPreferences(LIMIT_TO_READ, MODE_PRIVATE).edit();
                            editor.putInt(formattedDate, number + today);
                            editor.apply();
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        Log.d(TAG,listData.toString());
                        Fragment_list.update();
                    }
                });

    }

    private void query_ps_t(String startHash, String endHash, String primaryTaste, String secondPrimary, String mealType){
        mealRef.whereLessThan("geoHash",endHash).whereGreaterThan("geoHash",startHash).whereEqualTo("primaryTaste",primaryTaste).whereEqualTo("secondPrimary",secondPrimary)
                .whereEqualTo("mealType",mealType).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            int number=0;

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Log.d("GETTING DATA:","GETTING DATA");
                                Meal singlemeal=document.toObject(Meal.class);
                                listData.add(singlemeal);
                                databaseData.add(singlemeal);
                                //singlemeal.setLocalRelevance();
                                number += 1;
                                dbHelper.addData(singlemeal);
                            }
                            //shared prefs
                            editor = getSharedPreferences(LIMIT_TO_READ, MODE_PRIVATE).edit();
                            editor.putInt(formattedDate, number + today);
                            editor.apply();
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        Log.d(TAG,listData.toString());
                        Fragment_list.update();
                    }
                });

    }

    private void query_p(String startHash, String endHash, String primaryTaste){
        Log.d("QUERY PRIM:",startHash + " " + endHash + " " + primaryTaste);
        mealRef.whereLessThan("geoHash",endHash).whereGreaterThan("geoHash",startHash).whereEqualTo("primaryTaste",primaryTaste).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            int number=0;

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Log.d("GETTING DATA:","GETTING DATA");
                                Meal singlemeal=document.toObject(Meal.class);
                                listData.add(singlemeal);
                                databaseData.add(singlemeal);
                                //singlemeal.setLocalRelevance();
                                number += 1;
                                dbHelper.addData(singlemeal);
                            }
                            //shared prefs
                            editor = getSharedPreferences(LIMIT_TO_READ, MODE_PRIVATE).edit();
                            editor.putInt(formattedDate, number + today);
                            editor.apply();
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        Log.d(TAG,listData.toString());
                        Fragment_list.update();
                    }
                });

    }

    private void query_p_t(String startHash, String endHash, String primaryTaste, String mealType){
        Log.d("QUERY PRIM:",startHash + " " + endHash + " " + primaryTaste);
        mealRef.whereLessThan("geoHash",endHash).whereGreaterThan("geoHash",startHash).whereEqualTo("primaryTaste",primaryTaste)
                .whereEqualTo("mealType",mealType).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            int number=0;

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Log.d("GETTING DATA:","GETTING DATA");
                                Meal singlemeal=document.toObject(Meal.class);
                                listData.add(singlemeal);
                                databaseData.add(singlemeal);
                                //singlemeal.setLocalRelevance();
                                number += 1;
                                dbHelper.addData(singlemeal);
                            }
                            //shared prefs
                            editor = getSharedPreferences(LIMIT_TO_READ, MODE_PRIVATE).edit();
                            editor.putInt(formattedDate, number + today);
                            editor.apply();
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        Log.d(TAG,listData.toString());
                        Fragment_list.update();
                    }
                });

    }



    private void query__t(String startHash, String endHash, String mealType){
        Log.d("QUERY PRIM:",startHash + " " + endHash + " " + primaryTaste);
        mealRef.whereLessThan("geoHash",endHash).whereGreaterThan("geoHash",startHash)
                .whereEqualTo("mealType",mealType).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            int number=0;

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Log.d("GETTING DATA:","GETTING DATA");
                                Meal singlemeal=document.toObject(Meal.class);
                                listData.add(singlemeal);
                                databaseData.add(singlemeal);
                                //singlemeal.setLocalRelevance();
                                number += 1;
                                dbHelper.addData(singlemeal);
                            }
                            //shared prefs
                            editor = getSharedPreferences(LIMIT_TO_READ, MODE_PRIVATE).edit();
                            editor.putInt(formattedDate, number + today);
                            editor.apply();
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        Log.d(TAG,listData.toString());
                        Fragment_list.update();
                    }
                });

    }

    private void query__ts(String startHash, String endHash, String mealType, String mealSubType){
        Log.d("QUERY PRIM:",startHash + " " + endHash + " " + primaryTaste);
        mealRef.whereLessThan("geoHash",endHash).whereGreaterThan("geoHash",startHash).whereEqualTo("mealSubType",mealSubType)
                .whereEqualTo("mealType",mealType).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            int number=0;

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Log.d("GETTING DATA:","GETTING DATA");
                                Meal singlemeal=document.toObject(Meal.class);
                                listData.add(singlemeal);
                                databaseData.add(singlemeal);
                                //singlemeal.setLocalRelevance();
                                number += 1;
                                dbHelper.addData(singlemeal);
                            }
                            //shared prefs
                            editor = getSharedPreferences(LIMIT_TO_READ, MODE_PRIVATE).edit();
                            editor.putInt(formattedDate, number + today);
                            editor.apply();
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        Log.d(TAG,listData.toString());
                        Fragment_list.update();
                    }
                });

    }

    private void query_ps_ts(String startHash, String endHash, String primaryTaste, String secondPrimary, String mealType, String mealSubType){
        mealRef.whereLessThan("geoHash",endHash).whereGreaterThan("geoHash",startHash).whereEqualTo("primaryTaste",primaryTaste).whereEqualTo("secondPrimary",secondPrimary)
                .whereEqualTo("mealType",mealType).whereEqualTo("mealSubType",mealSubType).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            int number=0;

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Log.d("GETTING DATA:","GETTING DATA");
                                Meal singlemeal=document.toObject(Meal.class);
                                listData.add(singlemeal);
                                databaseData.add(singlemeal);
                                //singlemeal.setLocalRelevance();
                                number += 1;
                                dbHelper.addData(singlemeal);
                            }
                            //shared prefs
                            editor = getSharedPreferences(LIMIT_TO_READ, MODE_PRIVATE).edit();
                            editor.putInt(formattedDate, number + today);
                            editor.apply();
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        Log.d(TAG,listData.toString());
                        Fragment_list.update();
                    }
                });
    }

    private void query_p_ts(String startHash, String endHash, String primaryTaste, String mealType, String mealSubType){
        mealRef.whereLessThan("geoHash",endHash).whereGreaterThan("geoHash",startHash).whereEqualTo("primaryTaste",primaryTaste)
                .whereEqualTo("mealType",mealType).whereEqualTo("mealSubType",mealSubType).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            int number=0;

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Log.d("GETTING DATA:","GETTING DATA");
                                Meal singlemeal=document.toObject(Meal.class);
                                listData.add(singlemeal);
                                databaseData.add(singlemeal);
                                //singlemeal.setLocalRelevance();
                                number += 1;
                                dbHelper.addData(singlemeal);
                            }
                            //shared prefs
                            editor = getSharedPreferences(LIMIT_TO_READ, MODE_PRIVATE).edit();
                            editor.putInt(formattedDate, number + today);
                            editor.apply();
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        Log.d(TAG,listData.toString());
                        Fragment_list.update();
                    }
                });
    }

    
    public void loadDataMain(Double latitude,Double longitude, String primaryTaste, String secondPrimary,String mealType, String mealSubType){
        //shared prefs
        prefs = getSharedPreferences(LIMIT_TO_READ,MODE_PRIVATE);
        today = prefs.getInt(formattedDate,0);
        if (today == 0){
            prefs.edit().clear().apply();
        }
        else{
            Log.d("HOW MANY READS TODAY:",String.valueOf(today));
        }

        if (today >= 25){
            Toast.makeText(ViewActivity.this,"max read limit reached for today, please use local searches or upgrade to premium", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        //calculate northeast and southwest geohash
        Double latRadians = Math.toRadians(latitude);
        Double m_per_deg_lat = 111132.92 - 559.822 * Math.cos( 2.0 * latRadians ) + 1.175 * Math.cos( 4.0 * latRadians) - 0.0023 * Math.cos(6.0 * latRadians);
        Double m_per_deg_lon = (Math.PI/180.0) * 6367449 * Math.cos(latRadians);
        //Double m_per_deg_lon = 111412.84 * Math.cos ( latitude ) - 93.5 * Math.cos(3.0 * latitude) + 0.118 * Math.cos(5.0 * latitude);
        //8->38.2 and 19.1 ;;; 7-> 153 and 153
        Double latitudeDiv = 1.0/(Math.abs(m_per_deg_lat)/(123*iteration+123));
        Double longitudeDiv = 1.0/(Math.abs(m_per_deg_lon)/(123*iteration+123));
        Log.d("LATITUDE:",latitude.toString());
        Log.d("METERS PER DEGREE:",m_per_deg_lat.toString() + "<-lat;long->" + m_per_deg_lon.toString());

        Location location = new Location("geohash");
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        Location locationNE = new Location ("geohash");
        locationNE.setLatitude(latitude + latitudeDiv);
        locationNE.setLongitude(longitude + longitudeDiv);
        Location locationSW = new Location ("geohash");
        locationSW.setLatitude(latitude - latitudeDiv);
        locationSW.setLongitude(longitude - longitudeDiv);
        //Log.d("NE GEOHASH:",GeoHash.fromLocation(locationNE,9).toString());
        //Log.d("SW GEOHASH:",GeoHash.fromLocation(locationSW,9).toString());
        String startHash;
        String endHash;

        GeoHash hash = GeoHash.fromLocation(location, 9);
        Log.d("HASH:",hash.toString());

        endHash = GeoHash.fromLocation(locationNE,9).toString();
        startHash = GeoHash.fromLocation(locationSW,9).toString();

        if (iteration == 0){
            //startHash = hash.toString().substring(0,7) + "00";
            //endHash = hash.toString().substring(0,7) + "zz";
            //endHash = GeoHash.fromLocation(locationNE,9).toString();
            //startHash = GeoHash.fromLocation(locationSW,9).toString();
            Log.d("NE,SW HASHES:",startHash+ ";" + endHash);
            Log.d("HASHCODE:",startHash +  "," + endHash);
            Log.d("SEARCHING",primaryTaste + " " + secondPrimary + " " + mealType + " " + mealSubType);

            if ((primaryTaste==null || primaryTaste.equals("")) && (mealType==null || mealType.equals(""))){
                Log.d("LOAD NULL","everything is null");
                return;
            }
            else if ((primaryTaste!=null && !primaryTaste.equals("")) && (secondPrimary==null || secondPrimary.equals("")) && (mealType==null || mealType.equals("") || mealType.equals("unspecified"))){
                Log.d("QUERY:","p");
                query_p(startHash,endHash,primaryTaste);
            }
            else if ((primaryTaste!=null && !primaryTaste.equals("")) && (secondPrimary==null || secondPrimary.equals("")) && (mealType!=null && !mealType.equals("") && !mealType.equals("unspecified"))&& (mealSubType==null || mealSubType.equals("") || mealSubType.equals("unspecified"))){
                Log.d("QUERY:","p_t");
                query_p_t(startHash,endHash,primaryTaste,mealType);

            }
            else if ((primaryTaste!=null && !primaryTaste.equals("")) && (secondPrimary==null || secondPrimary.equals("")) && (mealType!=null && !mealType.equals("") && !mealType.equals("unspecified")) && (mealSubType!=null && !mealSubType.equals("") && !mealSubType.equals("unspecified"))){
                Log.d("QUERY:","p_ts");
                query_p_ts(startHash,endHash,primaryTaste,mealType,mealSubType);
            }
            else if ((primaryTaste!=null && !primaryTaste.equals("")) && (secondPrimary!=null && !secondPrimary.equals("")) && (mealType==null || mealType.equals("") || mealType.equals("unspecified"))){
                Log.d("QUERY:","ps");
                query_ps(startHash,endHash,primaryTaste,secondPrimary);
            }
            else if ((primaryTaste!=null && !primaryTaste.equals("")) && (secondPrimary!=null && !secondPrimary.equals("")) && (mealType!=null && !mealType.equals("") && !mealType.equals("unspecified")) && (mealSubType==null || mealSubType.equals("") || mealSubType.equals("unspecified"))){
                Log.d("QUERY:","ps_t");
                query_ps_t(startHash,endHash,primaryTaste,secondPrimary,mealType);
            }
            else if ((primaryTaste!=null && !primaryTaste.equals("")) && (secondPrimary!=null && !secondPrimary.equals("")) && (mealType!=null && !mealType.equals("") && !mealType.equals("unspecified")) && (mealSubType!=null && !mealSubType.equals("") && !mealSubType.equals("unspecified"))){
                Log.d("QUERY:","ps_ts");
                query_ps_ts(startHash,endHash,primaryTaste,secondPrimary,mealType,mealSubType);
            }
            else if ((primaryTaste==null || primaryTaste.equals("")) && (mealType!=null && !mealType.equals("") && !mealType.equals("unspecified")) && (mealSubType==null || mealSubType.equals("") || mealSubType.equals("unspecified"))){
                Log.d("QUERY:","_t");
                query__t(startHash,endHash,mealType);
            }
            else if ((primaryTaste==null || primaryTaste.equals("")) && (mealType!=null && !mealType.equals("") && !mealType.equals("unspecified")) && (mealSubType!=null && !mealSubType.equals("") && !mealSubType.equals("unspecified"))){
                Log.d("QUERY:","_ts");
                query__ts(startHash,endHash,mealType,mealSubType);
            }

            //Log.d("START.COMPARE(HASH)",String.valueOf(startHash.compareTo(hash.toString())));
            //Log.d("END.COMPARE(HASH)",String.valueOf(endHash.compareTo(hash.toString())));
            Log.d("PRIMARY TASTE:",primaryTaste);
            //query(startHash,endHash,primaryTaste,secondPrimary);
        }

        else{
            Log.d("LOAD DATA, iteration:",String.valueOf(iteration));
            // provjeri 9 polja po iteraciji
            /*startHash = hash.toString().substring(0,6) + "000";
            endHash = hash.toString().substring(0,6) + "zzz";
            Log.d("HASHCODE:",startHash +  "," + endHash);
            query_ps(startHash,firstStopHash,primaryTaste,secondPrimary);
            query_ps(secondStartHash,endHash,primaryTaste,secondPrimary);*/
            if ((primaryTaste==null || primaryTaste.equals("")) && (mealType==null || mealType.equals(""))){
                Log.d("LOAD NULL","everything is null");
                return;
            }
            else if ((primaryTaste!=null && !primaryTaste.equals("")) && (secondPrimary==null || secondPrimary.equals("")) && (mealType==null || mealType.equals("") || mealType.equals("unspecified"))){
                Log.d("QUERY:","p");
                query_p(startHash,firstStopHash,primaryTaste);
                query_p(secondStartHash,endHash,primaryTaste);
            }
            else if ((primaryTaste!=null && !primaryTaste.equals("")) && (secondPrimary==null || secondPrimary.equals("")) && (mealType!=null && !mealType.equals("") && !mealType.equals("unspecified"))&& (mealSubType==null || mealSubType.equals("") || mealSubType.equals("unspecified"))){
                Log.d("QUERY:","p_t");
                query_p_t(startHash,firstStopHash,primaryTaste,mealType);
                query_p_t(secondStartHash,endHash,primaryTaste,mealType);

            }
            else if ((primaryTaste!=null && !primaryTaste.equals("")) && (secondPrimary==null || secondPrimary.equals("")) && (mealType!=null && !mealType.equals("") && !mealType.equals("unspecified")) && (mealSubType!=null && !mealSubType.equals("") && !mealSubType.equals("unspecified"))){
                Log.d("QUERY:","p_ts");
                query_p_ts(startHash,firstStopHash,primaryTaste,mealType,mealSubType);
                query_p_ts(secondStartHash,endHash,primaryTaste,mealType,mealSubType);
            }
            else if ((primaryTaste!=null && !primaryTaste.equals("")) && (secondPrimary!=null && !secondPrimary.equals("")) && (mealType==null || mealType.equals("") || mealType.equals("unspecified"))){
                Log.d("QUERY:","ps");
                query_ps(startHash,firstStopHash,primaryTaste,secondPrimary);
                query_ps(secondStartHash,endHash,primaryTaste,secondPrimary);
            }
            else if ((primaryTaste!=null && !primaryTaste.equals("")) && (secondPrimary!=null && !secondPrimary.equals("")) && (mealType!=null && !mealType.equals("") && !mealType.equals("unspecified")) && (mealSubType==null || mealSubType.equals("") || mealSubType.equals("unspecified"))){
                Log.d("QUERY:","ps_t");
                query_ps_t(startHash,firstStopHash,primaryTaste,secondPrimary,mealType);
                query_ps_t(secondStartHash,endHash,primaryTaste,secondPrimary,mealType);
            }
            else if ((primaryTaste!=null && !primaryTaste.equals("")) && (secondPrimary!=null && !secondPrimary.equals("")) && (mealType!=null && !mealType.equals("") && !mealType.equals("unspecified")) && (mealSubType!=null && !mealSubType.equals("") && !mealSubType.equals("unspecified"))){
                Log.d("QUERY:","ps_ts");
                query_ps_ts(startHash,firstStopHash,primaryTaste,secondPrimary,mealType,mealSubType);
                query_ps_ts(secondStartHash,endHash,primaryTaste,secondPrimary,mealType,mealSubType);
            }
            else if ((primaryTaste==null || primaryTaste.equals("")) && (mealType!=null && !mealType.equals("") && !mealType.equals("unspecified")) && (mealSubType==null || mealSubType.equals("") || mealSubType.equals("unspecified"))){
                Log.d("QUERY:","_t");
                query__t(startHash,firstStopHash,mealType);
                query__t(secondStartHash,endHash,mealType);
            }
            else if ((primaryTaste==null || primaryTaste.equals("")) && (mealType!=null && !mealType.equals("") && !mealType.equals("unspecified")) && (mealSubType!=null && !mealSubType.equals("") && !mealSubType.equals("unspecified"))){
                Log.d("QUERY:","_ts");
                query__ts(startHash,firstStopHash,mealType,mealSubType);
                query__ts(secondStartHash,endHash,mealType,mealSubType);
            }
        }
        firstStopHash = startHash;
        secondStartHash = endHash;
        iteration += 1;

    }

    public void loadDataLocal(Double latitude,Double longitude){
        databaseData.clear();

        Double latRadians = Math.toRadians(latitude);
        Double m_per_deg_lat = 111132.92 - 559.822 * Math.cos( 2.0 * latRadians ) + 1.175 * Math.cos( 4.0 * latRadians) - 0.0023 * Math.cos(6.0 * latRadians);
        Double m_per_deg_lon = (Math.PI/180.0) * 6367449 * Math.cos(latRadians);
        //Double m_per_deg_lon = 111412.84 * Math.cos ( latitude ) - 93.5 * Math.cos(3.0 * latitude) + 0.118 * Math.cos(5.0 * latitude);
        //8->38.2 and 19.1 ;;; 7-> 153 and 153
        Double latitudeDiv;
        Double longitudeDiv;
        latitudeDiv = 1.0/(Math.abs(m_per_deg_lat)/(123*iteration+123));
        longitudeDiv = 1.0/(Math.abs(m_per_deg_lon)/(123*iteration+123));
        Log.d("LATITUDE:",latitude.toString());
        Log.d("METERS PER DEGREE:",m_per_deg_lat.toString() + "<-lat;long->" + m_per_deg_lon.toString());

        Location location = new Location("geohash");
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        Location locationNE = new Location ("geohash");
        locationNE.setLatitude(latitude + latitudeDiv);
        locationNE.setLongitude(longitude + longitudeDiv);
        Location locationSW = new Location ("geohash");
        locationSW.setLatitude(latitude - latitudeDiv);
        locationSW.setLongitude(longitude - longitudeDiv);

        String startHash = GeoHash.fromLocation(locationSW,9).toString();
        String endHash = GeoHash.fromLocation(locationNE,9).toString();

        //SQLiteDatabase database = dbHelper.getReadableDatabase();

        Cursor data = null;
        Log.d("LOADING DATA LOCALLY", "loadData: starting ");

        data = dbHelper.getDataASC(startHash,endHash);

        while(data.moveToNext()){
            //image handled in adapter
            Log.d("ADDING MEAL:", "meal " + data.getString(0));
            Log.d("MEAL NAME VIEW:",data.getString(35));
            Log.d("MEAL 15,16,17:",String.valueOf(data.getInt(15))+data.getString(16) + data.getDouble(17));
            Meal singleMeal = new Meal(data.getString(35),data.getDouble(1),data.getString(2),data.getInt(3),data.getString(4),
                    data.getString(5),data.getDouble(6),data.getInt(7),data.getDouble(8),data.getDouble(9),data.getString(10),
                    data.getString(11),data.getString(12),data.getString(13),data.getDouble(14),data.getInt(15),data.getString(16),
                    data.getDouble(17),data.getInt(18),data.getString(19),data.getDouble(20),data.getInt(21),
                    data.getString(22),data.getDouble(23),data.getInt(24),data.getDouble(25),data.getInt(26),data.getString(27),
                    data.getDouble(28),data.getInt(29),
                    data.getDouble(30),data.getInt(31),data.getString(32),data.getString(33),data.getString(34),
                    data.getString(36),data.getString(37),data.getString(38));
            if (!databaseData.contains(singleMeal)){
                databaseData.add(singleMeal);
            }
            /*if (!listData.contains(singleMeal)){
                //Log.d("BOOLEANS:",singleMeal.getGeoHash() + ";;start:" + startHash + " end:" + endHash );
                if ((singleMeal.getGeoHash().compareTo(startHash)>=0 || singleMeal.getGeoHash().compareTo(endHash)<=0)){
                    listData.add(singleMeal);
                }
            }*/
            Log.d("ADDING MEAL:",singleMeal.toString());

        }
        Fragment_list.update();
        iteration++;


    }

    public void reachedBottomOfListView(){
        if (type.equals("local")){
            loadDataLocal(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude));
            applyText(mealTypeA,mealSubTypeA,sweetA,sourA,saltA,bitterA,spicyA,umamiA,fullnessA);
        }
        else{
            if (secondPrimary== null){
                //loadData(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude),primaryTaste);
            }
            else{
                //loadData(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude),primaryTaste,secondPrimary);
            }
            loadDataMain(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude),primaryTaste,secondPrimary,mealTypeA,mealSubTypeA);
        }
    }



    //switch fragments
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selected = null;
            switch (item.getItemId()) {
                case R.id.navigation_list:
                    selected = new Fragment_list();
                    Log.d(TAG,"selected fragment list");
                    break;
                case R.id.navigation_mapa:
                    selected = new Fragment_map();
                    break;

            }

            replaceFragment(selected);

            return true;


        }
    };

    private void replaceFragment (Fragment fragment){

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,fragment).commit();
    }


    public void openDialog(){
        Log.d("VIEW ACTIVITY:","opening dialog");
        SearchDialog searchDialog=new SearchDialog();
        //Bundle args = new Bundle();
        //args.putSerializable("logs", listDataSpinner);
        //searchDialog.setArguments(args);
        searchDialog.show(getSupportFragmentManager(),"searchDialog");

    }

    //filter database data
    @Override
    public void applyText(String mealType, String mealSubType, String sweet, String sour,String salt, String bitter, String spicy, String umami,String fullness){
        /*Log.d(TAG, "applyText begin " + minPrice + " " + maxPrice + " " + sweet + " " + sour + " " + salt + " " + bitter + " " + spicy + " " + fullness );
        minPriceA = minPrice;
        maxPriceA = maxPrice;*/
        mealTypeA = mealType;
        mealSubTypeA = mealSubType;
        sweetA = sweet;
        sourA = sour;
        saltA = salt;
        bitterA = bitter;
        spicyA = spicy;
        umamiA = umami;
        fullnessA = fullness;


        listData.clear();
        for (Meal entry:databaseData){
            //Log.d(TAG, "applyText:2 " + entry.getMealName() + " " + entry.getMealName().toLowerCase().contains(entryName.toLowerCase()));
            //uvjet za search
            /*Double minPriceD=Double.parseDouble(minPrice);
            Double maxPriceD=Double.parseDouble(maxPrice);*/

            //Boolean tasteIsRight(Boolean isChangable,Double tasteEntry, String taste)

            Double priceEntry=entry.getusdPrice();
            Double sweetEntry=entry.getSweetTotal()/entry.getSweetNumber();
            Double sourEntry=entry.getSourTotal()/entry.getSourNumber();
            Double saltEntry=entry.getSaltTotal()/entry.getSaltNumber();
            Double bitterEntry=entry.getBitterTotal()/entry.getBitterNumber();
            Double spicyEntry=entry.getSpicyTotal()/entry.getSpicyNumber();
            Double fullnessEntry=entry.getFullnessTotal()/entry.getFullnessNumber();

            String mealTypeEntry = entry.getMealType();
            String mealSubTypeEntry = entry.getMealSubType();

            Log.d("SearchDialog","sweet");
            Boolean sweetIsRight = tasteIsRight(entry.getSweetChangeable(),sweetEntry,sweet);
            Log.d("SearchDialog","sour");
            Boolean sourIsRight = tasteIsRight(entry.getSourChangeable(),sourEntry,sour);
            Log.d("SearchDialog","salt");
            Boolean saltIsRight = tasteIsRight(entry.getSaltChangeable(),saltEntry,salt);
            Log.d("SearchDialog","bitter");
            Boolean bitterIsRight = tasteIsRight(false,bitterEntry,bitter);
            Log.d("SearchDialog","spicy");
            Boolean spicyIsRight = tasteIsRight(entry.getSpicyChangeable(),spicyEntry,spicy);
            Log.d("SearchDialog","fullness");
            Boolean fullnessIsRIght = tasteIsRight(false,fullnessEntry,fullness);


            if (sweetIsRight && sourIsRight && saltIsRight && bitterIsRight && spicyIsRight && fullnessIsRIght){
                if (mealType.equals("unspecified") || mealType.equals(mealTypeEntry)){
                    if (mealSubType.equals("")||mealSubType.equals("unspecified")|| mealSubType.equals(mealSubTypeEntry)){
                        listData.add(entry);
                    }
                }
            }


        }
        if (listData.equals(databaseData)){
            Log.d("SEARCH CHECK","All passing");
        }

        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if(f instanceof Fragment_map) {
            replaceFragment(new Fragment_map());
        }

        Fragment_list.update();

    }


    @Override
    public void reloadDataWithUpdatedParameters(String mealType, String mealSubType, String sweet, String sour, String salt, String bitter, String spicy, String umami, String fullness) {
        Log.d("RELOADDATAWUP:","reloading data");
        databaseData.clear();
        listData.clear();
        primaryTaste = null;
        secondPrimary = null;
        Double sweetTotal = Double.parseDouble(sweet);
        Double sourTotal = Double.parseDouble(sour);
        Double saltTotal = Double.parseDouble(salt);
        Double bitterTotal = Double.parseDouble(bitter);
        Double spicyTotal = Double.parseDouble(spicy);
        Double umamiTotal = Double.parseDouble(umami);
        Double tasteMax = Math.max(sweetTotal,Math.max(saltTotal,Math.max(sourTotal,Math.max(bitterTotal,Math.max(spicyTotal,umamiTotal)))));
        mealTypeA = mealType;
        mealSubTypeA = mealSubType;
        if (tasteMax==0){
            primaryTaste = "";
        }
        else if (sweetTotal.equals(tasteMax)){
            primaryTaste = "sweet";
        }
        else if (saltTotal.equals(tasteMax)){
            primaryTaste = "salt";
        }
        else if (sourTotal.equals(tasteMax)){
            primaryTaste = "sour";
        }
        else if (bitterTotal.equals(tasteMax)){
            primaryTaste = "bitter";
        }
        else if (spicyTotal.equals(tasteMax)){
            primaryTaste = "spicy";
        }
        else if (umamiTotal.equals(tasteMax)){
            primaryTaste = "umami";
        }

        if (!primaryTaste.equals("")){
            if (saltTotal.equals(tasteMax) && !primaryTaste.equals("salt")){
                secondPrimary = "salt";
            }
            else if (sourTotal.equals(tasteMax) && !primaryTaste.equals("sour")){
                secondPrimary = "sour";
            }
            else if (bitterTotal.equals(tasteMax) && !primaryTaste.equals("bitter")){
                secondPrimary = "bitter";
            }
            else if (spicyTotal.equals(tasteMax) && !primaryTaste.equals("spicy")){
                secondPrimary = "spicy";
            }
            else if (umamiTotal.equals(tasteMax) && !primaryTaste.equals("umami")){
                secondPrimary = "umami";
            }
            else{
                secondPrimary = null;
            }

        }
        if (type.equals("local")){
            loadDataLocal(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude));
            applyText(mealType,mealSubType,sweet,sour,salt,bitter,spicy,umami,fullness);
            Log.d("MEAL LOCAL INFO:",mealType + " " + mealSubType + " " + sweet + " " + sour + " " + umami);
        }
        else{
            /*
            if (mealType.equals("unspecified")){
                if (secondPrimary== null){
                    loadData(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude),primaryTaste);
                }
                else{
                    loadData(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude),primaryTaste,secondPrimary);
                }
            }
            else{
                if (mealSubType.equals("unspecified")||mealSubType.equals("")){
                    if (secondPrimary == null){
                        loadData(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude),primaryTaste,"",mealType);
                    }
                    else{
                        loadData(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude),primaryTaste,secondPrimary,mealType);
                    }
                }
                else{
                    if (secondPrimary == null){
                        loadData(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude),primaryTaste,"",mealType);
                    }
                    else{
                        loadData(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude),primaryTaste,secondPrimary,mealType);
                    }
                }
            }*/
            loadDataMain(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude),primaryTaste,secondPrimary,mealTypeA,mealSubTypeA);
        }
    }

    public void requestLocationUpdates(){
        Toast.makeText(ViewActivity.this,"Getting Location, please wait a few seconds", Toast.LENGTH_SHORT).show();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(ViewActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
        mLocationCallback=new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Toast.makeText(ViewActivity.this,"Location data acquired", Toast.LENGTH_SHORT).show();
                mlatitude=String.valueOf(locationResult.getLastLocation().getLatitude());
                Log.d(TAG,"latitude:"+ mlatitude);
                mlongitude=String.valueOf(locationResult.getLastLocation().getLongitude());
                Log.d(TAG,"longitude:"+ mlongitude);
                //loadData();

                //hashFUnction
                //loadData(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude));
                try{
                    fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
                    Log.d(TAG,"stopped location updates");
                }
                catch (Exception e){
                    e.printStackTrace();
                    Log.d(TAG,"failed to stop location updates");
                }

            }
        };
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PermissionChecker.PERMISSION_GRANTED){
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, mLocationCallback, getMainLooper());
        }

    }

    private Boolean tasteIsRight(Boolean isChangable,Double tasteEntry, String taste){
        Double tasteD;
        if (taste!=null){
            tasteD=Double.parseDouble(taste);
        }
        else{
            tasteD = 0.0;
        }
        // interval list
        Boolean tasteIsRight = abs(tasteEntry-tasteD)<2;
        if (isChangable){
            tasteIsRight = abs(tasteEntry-tasteD)<3;
        }
        if (tasteD==0){
            tasteIsRight=true;
            Log.d("SearchDialog","tasteD je 0");
        }
        Log.d("SearchDialog",tasteIsRight.toString());
        return tasteIsRight;


    }

    //locationPicker+placerPicker

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            /*old placePicker
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this,data);
                LatLng latLng = place.getLatLng();
                mlatitude = String.valueOf(latLng.latitude);
                mlongitude = String.valueOf(latLng.longitude);
                Toast.makeText(ViewActivity.this,mlongitude + " " + mlatitude, Toast.LENGTH_SHORT).show();
                resetData();


            }*/
            if (resultCode == Activity.RESULT_OK && data != null) {
                //LatLng latLng = data.getParcelableExtra(Constants.SHOW_LAT_LONG_INTENT);
                AddressData addressData = data.getParcelableExtra(Constants.ADDRESS_INTENT);
                mlatitude = String.valueOf(addressData.getLatitude());
                mlongitude = String.valueOf(addressData.getLongitude());
                Toast.makeText(ViewActivity.this,mlongitude + " " + mlatitude, Toast.LENGTH_SHORT).show();
                resetData();

                //Log.d("LATLONG:",addressData.toString());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void resetData(){
        databaseData.clear();
        listData.clear();
        iteration = 0;

        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if(f instanceof Fragment_map) {
            replaceFragment(new Fragment_map());
        }

        Fragment_list.update();
        if (type.equals("local")){
            loadDataLocal(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude));
            applyText("0","9999999","0","0","0","0","0","0","0");
        }
        else{
            /*
            if (secondPrimary == null){
                loadData(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude),primaryTaste);
            }
            else{
                loadData(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude),primaryTaste,secondPrimary);
            }*/
            loadDataMain(Double.parseDouble(mlatitude),Double.parseDouble(mlongitude),primaryTaste,secondPrimary,mealTypeA,mealSubTypeA);
        }

    }
}
