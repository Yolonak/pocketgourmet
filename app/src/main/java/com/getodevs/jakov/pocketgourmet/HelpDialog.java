package com.getodevs.jakov.pocketgourmet;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDialogFragment;

public class HelpDialog extends AppCompatDialogFragment {
    private TextView helpTV;
    private String text;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        text = getArguments().getString("activity");


        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.layout_help_dialog,null);

        builder.setView(view).setTitle("Help").setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("OK , but on the right side", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                /*Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + "getodevs@gmail.com"));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[PocketGourmet]");

                startActivity(Intent.createChooser(emailIntent, "Send us an e-mail"));*/
            }
        });

        helpTV = view.findViewById(R.id.helpTV);

        helpTV.setMovementMethod(new ScrollingMovementMethod());
        helpTV.setText(text);
        return builder.create();



    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }
}