package com.getodevs.jakov.pocketgourmet;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.content.PermissionChecker;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.Manifest;

import com.fonfon.geohash.GeoHash;
import com.getodevs.jakov.pocketgourmet.localstorage.DatabaseHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
//import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
//import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
//import com.google.android.gms.location.places.Place;
//import com.google.android.gms.location.places.ui.PlacePicker;
//import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.sucho.placepicker.AddressData;
import com.sucho.placepicker.Constants;
import com.sucho.placepicker.PlacePicker;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.annotation.Nullable;


public class AddMealActivity extends AppCompatActivity implements timeInfoDialog.timeInfoDialogListener{

    private Button getMyLocationBtn, selectCustomLocationBtn, addMealBtn;
    private TextView latitudeTextView, longitudeTextView, addressTextView;
    private EditText titleEditText,priceEditText;
    private SeekBar sweetTasteSeekBar,sourTasteSeekBar,saltTasteSeekBar,bitterTasteSeekBar,spicyTasteSeekBar, umamiTasteSeekBar, fullnessSeekBar;
    private CheckBox sweetCB,sourCB,saltCB,spicyCB;
    private Spinner currencySpinner,mealTypeSpinner,mealSubTypeSpinner;
    private static final int REQUEST_LOCATION = 1;
    private static final int GALLERY_REQUEST_CODE=200;
    private static final int CAMERA_REQUEST_CODE=300;
    private String latitude, longitude;
    private ImageView imageView;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private FirebaseStorage storage=FirebaseStorage.getInstance();
    int PLACE_PICKER_REQUEST = 1;
    PlacePicker.IntentBuilder builder;
    LocationCallback mLocationCallback;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    private ArrayAdapter<CharSequence> adapter;


    //LOCAL STORAGE
    DatabaseHelper dbHelper;

    private String cameraFilePath;

    //TIME INFO DIALOG
    private Button addTimeInfoBtn;
    private String workdayHours,saturdayHours,sundayHours = "unknown";

    //duplicates
    ArrayList<Meal> databaseData;
    int duplicates;
    CollectionReference mealRef=db.collection("meals");




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_meal);

        //Log.d("CURRENT USERNAME:",user.getDisplayName());
        fusedLocationProviderClient = new FusedLocationProviderClient(this);
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setFastestInterval(10000);
        locationRequest.setInterval(12000);


        sweetCB=(CheckBox)findViewById(R.id.sweetCB);
        sourCB=(CheckBox)findViewById(R.id.sourCB);
        saltCB=(CheckBox)findViewById(R.id.saltCB);
        spicyCB=(CheckBox)findViewById(R.id.spicyCB);

        sweetTasteSeekBar=(SeekBar)findViewById(R.id.sweetTasteSeekBar);
        sourTasteSeekBar=(SeekBar)findViewById(R.id.sourTasteSeekBar);
        saltTasteSeekBar=(SeekBar)findViewById(R.id.saltTasteSeekBar);
        bitterTasteSeekBar=(SeekBar)findViewById(R.id.bitterTasteSeekBar);
        spicyTasteSeekBar=(SeekBar)findViewById(R.id.spicyTasteSeekBar);
        fullnessSeekBar=(SeekBar)findViewById(R.id.fullnessSeekBar);
        umamiTasteSeekBar = (SeekBar)findViewById(R.id.umamiTasteSeekBar);

        priceEditText=(EditText)findViewById(R.id.priceEditText);
        titleEditText=(EditText)findViewById(R.id.titleEditText);

        addMealBtn=(Button)findViewById(R.id.addMealBtn);
        getMyLocationBtn=(Button)findViewById(R.id.getMyLocationBtn);
        selectCustomLocationBtn=(Button)findViewById(R.id.selectCustomLocationBtn);

        latitudeTextView=(TextView)findViewById(R.id.latitudeTextView);
        longitudeTextView=(TextView)findViewById(R.id.longitudeTextView);
        addressTextView=(TextView)findViewById(R.id.addressTextView);
        imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.defpic);
        currencySpinner=(Spinner)findViewById(R.id.currencySpinner);
        mealTypeSpinner=(Spinner)findViewById(R.id.mealTypeSpinner);
        mealSubTypeSpinner=(Spinner)findViewById(R.id.mealSubTypeSpinner);

        //TIME DIALOG
        addTimeInfoBtn=(Button)findViewById(R.id.addTimeInfoBtn);

        //local storage
        dbHelper = new DatabaseHelper(this);


        setOnClickListeners();
        setSpinnerListener();
        initialize();
    }
    @Override
    protected void onPause() {

        super.onPause();
        try{
            fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setOnClickListeners(){
        getMyLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                requestLocationUpdates();

            }
        });
        if(isServicesOK()){
            selectCustomLocationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*oldPlacePicker
                    try{
                        fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    builder = new PlacePicker.IntentBuilder();
                    try {
                        startActivityForResult(builder.build(AddMealActivity.this), PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    }*/

                    builder = new PlacePicker.IntentBuilder()
                            .setLatLong(40.748672, -73.985628)  // Initial Latitude and Longitude the Map will load into
                            .showLatLong(true)  // Show Coordinates in the Activity
                            .setMapZoom(12.0f)  // Map Zoom Level. Default: 14.0
                            .setAddressRequired(true) // Set If return only Coordinates if cannot fetch Address for the coordinates. Default: True
                            .hideMarkerShadow(true); // Hides the shadow under the map marker. Default: False
                            /*
                            .setMarkerDrawable(R.drawable.marker) // Change the default Marker Image
                            .setMarkerImageImageColor(R.color.colorPrimary)
                            .setFabColor(R.color.fabColor)
                            .setPrimaryTextColor(R.color.primaryTextColor) // Change text color of Shortened Address
                            .setSecondaryTextColor(R.color.secondaryTextColor) // Change text color of full Address*/
                    //.build(getActivity());

                    startActivityForResult(builder.build(AddMealActivity.this), PLACE_PICKER_REQUEST);
                }
            });

        }


        addTimeInfoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimeDialog();
            }
        });


        addMealBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(titleEditText.getText().toString().trim())) {
                    Toast.makeText(getApplicationContext(), "Enter meal name!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (latitudeTextView.getText().toString().trim().substring(9).equals("latitude:")||longitudeTextView.getText().toString().trim().substring(10).equals("longitude:")||addressTextView.getText().toString().trim().equals("address:")) {
                    Toast.makeText(getApplicationContext(), "Missing location or address!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(priceEditText.getText().toString().trim())) {
                    Toast.makeText(getApplicationContext(), "Enter price!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (imageView.getDrawable()==null){
                    Toast.makeText(getApplicationContext(), "cannot create meal with no image!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Thread myThread=new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final String mealName=titleEditText.getText().toString().trim();
                        final String priceFinal=priceEditText.getText().toString().trim();
                        final String createdAt = Calendar.getInstance().getTime().toString().trim();
                        final String createdBy=user.getEmail().toString().trim();
                        final String latitudeFinal=latitudeTextView.getText().toString().trim().substring(9);
                        final String longitudeFinal=longitudeTextView.getText().toString().trim().substring(10);
                        final String addressFinal=addressTextView.getText().toString().trim();

                        final Integer sweet=sweetTasteSeekBar.getProgress();
                        final Integer sour=sourTasteSeekBar.getProgress();
                        final Integer salt=saltTasteSeekBar.getProgress();
                        final Integer bitter=bitterTasteSeekBar.getProgress();
                        final Integer spicy=spicyTasteSeekBar.getProgress();
                        final Integer fullness=fullnessSeekBar.getProgress();
                        final Integer umami = umamiTasteSeekBar.getProgress();
                        //final Double longitudeArea= Double.valueOf(Math.round(Double.parseDouble(longitudeTextView.getText().toString().trim().substring(10))*10));

                        final Boolean sweetChangeable=sweetCB.isChecked();
                        final Boolean sourChangeable=sourCB.isChecked();
                        final Boolean saltChangeable=saltCB.isChecked();
                        final Boolean spicyChangeable=spicyCB.isChecked();
                        final String currency = currencySpinner.getSelectedItem().toString();
                        final String mealType = mealTypeSpinner.getSelectedItem().toString();
                        final String mealSubType = mealSubTypeSpinner.getSelectedItem().toString();

                        //GeoHash

                        Location location = new Location("geohash");
                        location.setLatitude(Double.parseDouble(latitudeFinal));
                        location.setLongitude(Double.parseDouble(longitudeFinal));

                        final GeoHash hash = GeoHash.fromLocation(location, 9);
                        Log.d("HASHCODE:",hash.toString());



                        Log.d("addMealBtn","useremail:"+createdBy);
                        Log.d("addMealBtn","time:"+createdAt);
                        Log.d("addMealBtn","passed ifs");
                        Log.d("addMealBtn","sweet:"+sweet.toString());
                        Log.d("addMealBtn","sour:"+sour.toString());
                        Log.d("addMealBtn","salt:"+salt.toString());
                        Log.d("addMealBtn","bitter:"+bitter.toString());
                        Log.d("addMealBtn","spicy:"+spicy.toString());
                        Log.d("addMealBtn","latitude:"+latitudeFinal);
                        Log.d("addMealBtn","longitude:"+longitudeFinal);
                        if (sweetChangeable){
                            Log.d("addMealBtn","sweetChangeable");
                        }

                        checkForDuplicates(hash.toString(), mealName, Double.parseDouble(priceFinal), new OnSuccessListener<Boolean>() {
                            @Override
                            public void onSuccess(Boolean aBoolean) {
                                if (aBoolean){
                                    imageView.setDrawingCacheEnabled(true);
                                    imageView.buildDrawingCache();
                                    Bitmap bitmap=imageView.getDrawingCache();
                                    ByteArrayOutputStream baos=new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.JPEG,60,baos);
                                    imageView.setDrawingCacheEnabled(false);
                                    byte[] data=baos.toByteArray();
                                    String imagePath="newMealImages/" + UUID.randomUUID() + ".jpg";
                                    StorageReference newMealImagesRef=storage.getReference(imagePath);
                                    UploadTask uploadTask = newMealImagesRef.putBytes(data);
                                    final Meal meal= new Meal (mealName,Double.parseDouble(priceFinal),currency,createdAt,createdBy,Double.parseDouble(latitudeFinal),Double.parseDouble(longitudeFinal),/*longitudeArea,*/addressFinal,hash.toString(),
                                            workdayHours,saturdayHours,sundayHours,sweet.doubleValue(),sweetChangeable,sour.doubleValue(), sourChangeable,salt.doubleValue(),saltChangeable,bitter.doubleValue(),spicy.doubleValue(),spicyChangeable,umami.doubleValue(),fullness.doubleValue(),mealType,imagePath,mealSubType);
                                    AddMeal(meal);
                                    AddMealLocally(meal);
                                    Log.d("ADDING MEAL:",meal.getSaltNumber().toString() + " " + meal.getSaltTotal().toString());

                                }
                                else{
                                    Toast.makeText(AddMealActivity.this, "Meal with same name,price and location exists", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        /*
                        imageView.setDrawingCacheEnabled(true);
                        imageView.buildDrawingCache();
                        Bitmap bitmap=imageView.getDrawingCache();
                        ByteArrayOutputStream baos=new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG,60,baos);
                        imageView.setDrawingCacheEnabled(false);
                        byte[] data=baos.toByteArray();
                        String imagePath="newMealImages/" + UUID.randomUUID() + ".jpg";
                        StorageReference newMealImagesRef=storage.getReference(imagePath);
                        UploadTask uploadTask = newMealImagesRef.putBytes(data);
                        final Meal meal= new Meal (mealName,Double.parseDouble(priceFinal),currency,createdAt,createdBy,Double.parseDouble(latitudeFinal),Double.parseDouble(longitudeFinal),/*longitudeArea,addressFinal,hash.toString(),
                                workdayHours,saturdayHours,sundayHours,sweet.doubleValue(),sweetChangeable,sour.doubleValue(), sourChangeable,salt.doubleValue(),saltChangeable,bitter.doubleValue(),spicy.doubleValue(),spicyChangeable,fullness.doubleValue(),mealType,imagePath);
                        AddMeal(meal);
                        AddMealLocally(meal);
                        Log.d("ADDING MEAL:",meal.getSaltNumber().toString() + " " + meal.getSaltTotal().toString());*/

                    }
                });
                myThread.start();
                finish();

            }
        });

    }

    public void requestLocationUpdates(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(AddMealActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
        mLocationCallback=new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                latitude=String.valueOf(locationResult.getLastLocation().getLatitude());
                longitude=String.valueOf(locationResult.getLastLocation().getLongitude());

                latitudeTextView.setText("Latitude:" + latitude);
                longitudeTextView.setText("Longitude:" + longitude);
                addressTextView.setText(getCompleteAddressString(Double.parseDouble(latitude),Double.parseDouble(longitude)));
                /*try{
                    fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
                }
                catch (Exception e){
                    e.printStackTrace();
                }*/
            }
        };
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PermissionChecker.PERMISSION_GRANTED){
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, mLocationCallback, getMainLooper());
        }

    }

    public boolean isServicesOK(){
        Log.d("SearchActivity", "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(AddMealActivity.this);

        if(available == ConnectionResult.SUCCESS){
            //everything is fine and the user can make map requests
            Log.d("SearchActivity", "isServicesOK: Google Play Services is working");
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available)){
            //an error occured but we can resolve it
            Log.d("SearchActivity", "isServicesOK: an error occured but we can fix it");
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        if (requestCode == PLACE_PICKER_REQUEST) {
            /*OLD PLACEPICKER
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this,data);
                LatLng latLng = place.getLatLng();
                latitude = String.valueOf(latLng.latitude);
                longitude = String.valueOf(latLng.longitude);
                //Toast.makeText(getApplicationContext(), latitude + " " + longitude, Toast.LENGTH_SHORT).show();
                latitudeTextView.setText("Latitude:" + latitude);
                longitudeTextView.setText("Longitude:" + longitude);
                addressTextView.setText(getCompleteAddressString(Double.parseDouble(latitude),Double.parseDouble(longitude)));


            }*/
            if (resultCode == Activity.RESULT_OK && data != null) {
                //LatLng latLng = data.getParcelableExtra(Constants.SHOW_LAT_LONG_INTENT);
                AddressData addressData = data.getParcelableExtra(Constants.ADDRESS_INTENT);
                latitude = String.valueOf(addressData.getLatitude());
                longitude = String.valueOf(addressData.getLongitude());
                latitudeTextView.setText("Latitude:" + latitude);
                longitudeTextView.setText("Longitude:" + longitude);
                addressTextView.setText(getCompleteAddressString(Double.parseDouble(latitude),Double.parseDouble(longitude)));


                //Log.d("LATLONG:",addressData.toString());
            }
            else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
        else if(requestCode==GALLERY_REQUEST_CODE){
            if(resultCode==RESULT_OK){
                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
                //Get the column index of MediaStore.Images.Media.DATA
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                //Gets the String value in the column
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();
                // Set the Image in ImageView after decoding the String
                imageView.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                Log.d("onActivityResult","calledOnActivityResultRESULTOK");
            }
            Log.d("onActivityResult","calledOnActivityResult");
        }
        else if(requestCode==CAMERA_REQUEST_CODE){
            if(resultCode==RESULT_OK){
                imageView.setImageURI(Uri.parse(cameraFilePath));
                Log.d("onActivityResult","calledOnActivityResultCAMERA");
            }
        }
    }
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {

        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current address", strReturnedAddress.toString());
            } else {
                Log.w("My Current address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current address", "Can't get Address!");
        }
        return strAdd;
    }
    private void verifyPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permissions = {android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA};

            if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    permissions[0]) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    permissions[1]) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    permissions[2]) == PackageManager.PERMISSION_GRANTED) {
            } else {
                ActivityCompat.requestPermissions(AddMealActivity.this,
                        permissions,
                        1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String[] permissions,  int[] grantResults) {
        //verifyPermissions();
    }



    protected void initialize() {
        verifyPermissions();
        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                pickFromGallery();
                return true;
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureFromCamera();

            }
        });
    }
    private void pickFromGallery(){
        //Create an Intent with action as ACTION_PICK
        Intent intent=new Intent(Intent.ACTION_PICK);
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.setType("image/*");
        //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
        // Launching the Intent
        startActivityForResult(intent,GALLERY_REQUEST_CODE);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //This is the directory in which the file will be created. This is the default location of Camera photos
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for using again
        cameraFilePath = "file://" + image.getAbsolutePath();
        return image;
    }
    private void captureFromCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", createImageFile()));
            startActivityForResult(intent, CAMERA_REQUEST_CODE);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void AddMeal(Meal meal){
        Log.d("AddingMeal","meal is good to go");
        db.collection("meals")
                .add(meal)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("AddingMeal","DocumentSnapshot added with ID:" + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("AddingMeal", "Error adding document", e);
                    }
                });

    }

    public void AddMealLocally(Meal meal){
        boolean insert = dbHelper.addData(meal);
    }

    public void openTimeDialog(){
        timeInfoDialog timeInfoDialog = new timeInfoDialog();
        timeInfoDialog.show(getSupportFragmentManager(),"timeInfoDialog");

    }

    @Override
    public void applyTimeInfo(String WDO, String WDC, String STO, String STC, String SNO, String SNC) {
        Log.d("TimePassed",WDO + " " + WDC + " " + STO +" " + STC + " " + SNO + " " + SNC);
        workdayHours = WDO + "-" + WDC;
        saturdayHours = STO + "-" + STC;
        sundayHours = SNO + "-" + SNC;
    }

    public void checkForDuplicates(String geoHash, String name, Double price, final OnSuccessListener<Boolean> onSuccessListener){
        String startHash = geoHash.substring(0,7) + "00";
        String endHash = geoHash.substring(0,7) + "zz";
        databaseData = new ArrayList<Meal>();
        duplicates = 0;


        mealRef.whereLessThan("geoHash",endHash).whereGreaterThan("geoHash",startHash).whereEqualTo("mealName",name).whereEqualTo("usdPrice",price)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    private boolean boolDuplicate=false;
                    @Override
                    public void onEvent(QuerySnapshot queryDocumentSnapshots,FirebaseFirestoreException e) {
                        if (!boolDuplicate){
                            boolDuplicate = true;
                            if (e != null) {
                                e.printStackTrace();
                                String message = e.getMessage();
                                onSuccessListener.onSuccess(false);
                                return;
                            }
                            else{
                                List<DocumentSnapshot> snapshotList = queryDocumentSnapshots.getDocuments();
                                if (snapshotList.size() > 0) {
                                    //Field is Exist
                                    onSuccessListener.onSuccess(false);
                                } else {
                                    onSuccessListener.onSuccess(true);
                                }
                            }

                        }
                    }
                });
    }

    private void setSpinnerListener(){
        mealSubTypeSpinner.setAdapter(adapter);
        mealTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("ITEM SELECTED:", parent.getItemAtPosition(position).toString());
                String type = parent.getItemAtPosition(position).toString();
                switch (type) {
                    case "healthy":
                        Log.d("IN CASE:", "healthy");
                        adapter = ArrayAdapter.createFromResource(AddMealActivity.this, R.array.healthy, android.R.layout.simple_spinner_item);
                        adapter.notifyDataSetChanged();
                        mealSubTypeSpinner.setAdapter(adapter);
                        return;
                    case "BBQ":
                        Log.d("IN CASE:", "BBQ");
                        adapter = ArrayAdapter.createFromResource(AddMealActivity.this, R.array.BBQ, android.R.layout.simple_spinner_item);
                        adapter.notifyDataSetChanged();
                        mealSubTypeSpinner.setAdapter(adapter);
                        return;

                }
            }



            //mealSubTypeSpinner.

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}

