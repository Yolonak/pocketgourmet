package com.getodevs.jakov.pocketgourmet;

import android.Manifest;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoggedInActivity extends AppCompatActivity {

    private Button logOutBtn,profileBtn,addMealBtn,viewAllMealsBtn,addMealStepBtn,viewLocalMeals,helpBtn;
    private FirebaseAuth auth;
    private String TAG="LoggedInActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_in);
        auth=FirebaseAuth.getInstance();
        Log.d(TAG,"current user:"+ auth.getCurrentUser().toString());
        logOutBtn=(Button)findViewById(R.id.logOutBtn);
        profileBtn=(Button)findViewById(R.id.profileBtn);
        addMealBtn=(Button)findViewById(R.id.addMealBtn);
        viewAllMealsBtn=(Button)findViewById(R.id.viewAllMealsBtn);
        addMealStepBtn=(Button)findViewById(R.id.addMealStepBtn);
        viewLocalMeals=(Button)findViewById(R.id.viewLocalMeals);
        helpBtn=(Button)findViewById(R.id.helpBtn);


        setListeners();



    }
    public void setListeners(){
        FirebaseAuth.AuthStateListener authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    // user auth state is changed - user is null
                    // launch login activity
                    startActivity(new Intent(LoggedInActivity.this, LogInActivity.class));
                    finish();
                }
            }
        };

        logOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auth.signOut();
                startActivity(new Intent(LoggedInActivity.this, LogInActivity.class));
                finish();
            }
        });
        profileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoggedInActivity.this, ManageAccActivity.class));
            }
        });
        addMealBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkIfEmailVerified()){
                    startActivity(new Intent(LoggedInActivity.this, AddMealActivity.class));
                }
            }
        });
        viewAllMealsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoggedInActivity.this, ViewActivity.class);
                intent.putExtra("type", "internet");
                startActivity(intent);
            }
        });
        addMealStepBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startActivity(new Intent(LoggedInActivity.this, AddMealStepActivity.class));
            }
        });
        viewLocalMeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoggedInActivity.this, ViewActivity.class);
                intent.putExtra("type", "local");
                startActivity(intent);
            }
        });
        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HelpDialog helpDialog = new HelpDialog();
                Bundle args = new Bundle();
                args.putString("activity","LoggedInActivity");
                helpDialog.setArguments(args);
                helpDialog.show(getSupportFragmentManager(),"helpDialog");
            }
        });




    }



    //check user verification
    private boolean checkIfEmailVerified()
    {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user!=null && user.isEmailVerified())
        {
            // user is verified, so you can finish this activity or send user to activity which you want.
            //finish();
            Toast.makeText(LoggedInActivity.this, "UserIsVerified", Toast.LENGTH_SHORT).show();
            return true;
        }
        else
        {
            // email is not verified, so just prompt the message to the user and restart this activity.
            // NOTE: don't forget to log out the user.
            Toast.makeText(LoggedInActivity.this, "UserIsNotVerified; verify and then log in again", Toast.LENGTH_SHORT).show();
            sendVerificationEmail();


            return false;

            //restart this activity

        }
    }

    private void sendVerificationEmail()
    {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        /*
        if (user == null){
            finish();
            return;
        }*/

        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // email sent


                            // after email is sent just logout the user and finish this activity
                            //FirebaseAuth.getInstance().signOut();
                            //startActivity(new Intent(RegisterActivity.this, LogInActivity.class));
                            //finish();
                            auth.signOut();
                            startActivity(new Intent(LoggedInActivity.this, LogInActivity.class));
                        }
                        else
                        {
                            // email not sent, so display message and restart the activity or do whatever you wish to do

                            //restart this activity
                            overridePendingTransition(0, 0);
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(getIntent());

                        }
                    }
                });
    }


    /*private void verifyPermissions(){
        if (Build.VERSION.SDK_INT >= 23) {
            String[] permissions = {Manifest.permission.INTERNET,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.CAMERA};
            Log.d("permissionLog","1");
            if(ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    permissions[0]) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    permissions[1]) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    permissions[2]) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    permissions[3]) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    permissions[4]) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    permissions[5]) == PackageManager.PERMISSION_GRANTED){
                Log.d(TAG,"verified all permissions");
            }else{
                ActivityCompat.requestPermissions(LoggedInActivity.this,
                        permissions,
                        1);
                Log.d(TAG,"some missing");
            }
        } else {
            Log.d(TAG,"sdk less than 23");
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d("grantResult", grantResults.toString());

        if (grantResults.length > 0) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.d("permissionLog", "5");
            }
        }
    }*/
}
