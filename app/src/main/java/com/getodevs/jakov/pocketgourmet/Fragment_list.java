package com.getodevs.jakov.pocketgourmet;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Fragment_list extends Fragment implements  AdapterView.OnItemClickListener{
    private static final String TAG = "Fragment_list";
    ListView mListView;
    static MealListAdapter adapter;
    public static ArrayList<Meal> listData;
    ArrayList<Meal> listData_selected;
    int count;
    String mealKey;

    public static void update() {
        Log.d("fragment_list", "update: ");
        adapter.notifyDataSetChanged();
    }

    @SuppressLint("MissingPermission")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        mListView = view.findViewById(R.id.mListView);
        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListView.setSelector(android.R.color.transparent);
        mListView.setDivider(null);
        mListView.setDividerHeight(35);
        mListView.setOnItemClickListener(this);
        listData_selected=new ArrayList<>();
        count=0;
        listData=((ViewActivity) getActivity()).getListData();
        Log.d(TAG, "created view");
        Log.d(TAG, listData.toString());




        mListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                MenuInflater inflater = actionMode.getMenuInflater();
                inflater.inflate(R.menu.context_menu,menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.selectAll_id:
                        int n = listData.size();
                        if (listData_selected.size() == n) {
                            for (int i=0;i < n; i++) {
                                mListView.setItemChecked(i, false);
                            }
                        }
                        else {
                            for (int i=0;i < n; i++) {
                                if (!listData_selected.contains(listData.get(i))) {
                                    mListView.setItemChecked(i, true);
                                }
                            }
                        }
                        return true;
                    case R.id.delete_id:
                        for (Meal item : listData_selected){
                            adapter.remove(item);
                            //((ViewActivity) getActivity()).mDatabaseHelper.deleteEntry(timestamp);

                        }
                        Toast.makeText(((ViewActivity) getActivity()).getBaseContext(),count+" removed",Toast.LENGTH_SHORT).show();
                        count=0;
                        actionMode.finish();
                        adapter.notifyDataSetChanged();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {
                listData_selected.clear();
                count = 0;
            }

            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {

                if (listData_selected.contains(listData.get(i))){
                    count = count-1;
                    listData_selected.remove(listData.get(i));
                    actionMode.setTitle(count + " Items selected");
                }
                else{

                    listData_selected.add(listData.get(i));
                    count += 1;
                    actionMode.setTitle(count + " Items selected");
                }


            }
        });

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                //(scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && (mListView.getLastVisiblePosition() - mListView.getHeaderViewsCount() - mListView.getFooterViewsCount()) >= (adapter.getCount()-1))
                if (!view.canScrollList(View.SCROLL_AXIS_VERTICAL) && scrollState == SCROLL_STATE_IDLE){
                    Log.d("LISTVIEWFRAGMENT:","hit the bottom");
                    ((ViewActivity) getActivity()).reachedBottomOfListView();

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });


        adapter = new MealListAdapter(getActivity(), R.layout.meal_list_view_adapter, listData);
        mListView.setAdapter(adapter);


        return view;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        openDialogActivity(position);
    }

    private void openDialogActivity(int position) {
        Intent intent = new Intent(getActivity(),DialogActivity.class);
        Meal entry = ((ViewActivity) getActivity()).getListData().get(position);

        intent.putExtra("name",entry.getMealName());
        //intent.putExtra("description",entry.getDescription());
        intent.putExtra("path",entry.getPath());
        intent.putExtra("price",entry.getusdPrice().toString());
        intent.putExtra("latitude",entry.getLatitude().toString());
        intent.putExtra("longitude",entry.getLongitude().toString());
        intent.putExtra("address",entry.getAddress());
        intent.putExtra("position",position);
        intent.putExtra("activity","LogView");
        Log.d(TAG,entry.getLongitude().toString() + "long" + entry.getLatitude().toString() + "lat");
        startActivity(intent);
    }

}
