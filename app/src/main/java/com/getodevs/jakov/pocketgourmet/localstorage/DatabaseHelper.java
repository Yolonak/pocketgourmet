package com.getodevs.jakov.pocketgourmet.localstorage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.getodevs.jakov.pocketgourmet.Meal;

public class DatabaseHelper extends SQLiteOpenHelper {


    private static final String TABLE_NAME = "meals";
    private static final String COL0 = "mealKey";
    private static final String COL1 = "price";
    private static final String COL2 = "currency";
    private static final String COL3 = "timesVerified";
    private static final String COL4 = "createdAt";
    private static final String COL5 = "createdBy";
    private static final String COL6 = "mealRatingTotal";
    private static final String COL7 = "numberOfReviews";
    private static final String COL8 = "latitude";
    private static final String COL9 = "longitude";
    private static final String COL10 = "address";
    private static final String COL11 = "workDayHours";
    private static final String COL12 = "saturdayHours";
    private static final String COL13 = "sundayHours";
    private static final String COL14 = "sweetTotal";
    private static final String COL15 = "sweetNumber";
    private static final String COL16 = "sweetChangeable";
    private static final String COL17 = "sourTotal";
    private static final String COL18 = "sourNumber";
    private static final String COL19 = "sourChangeable";
    private static final String COL20 = "saltTotal";
    private static final String COL21 = "saltNumber";
    private static final String COL22 = "saltChangeable";
    private static final String COL23 = "bitterTotal";
    private static final String COL24 = "bitterNumber";
    private static final String COL25 = "spicyTotal";
    private static final String COL26 = "spicyNumber";
    private static final String COL27 = "spicyChangeable";
    private static final String COL28 = "umamiTotal";
    private static final String COL29 = "umamiNUmber";
    private static final String COL30 = "fullnessTotal";
    private static final String COL31 = "fullessNumber";
    private static final String COL32 = "mealType";
    private static final String COL33 = "path";
    private static final String COL34 = "geoHash";
    private static final String COL35 = "mealName";
    private static final String COL36 = "primaryTaste";
    private static final String COL37 = "secondPrimary";
    private static final String COL38 = "mealSubType";



    private Context appContext;

    public DatabaseHelper(Context context, String tablename) {
        super(context,tablename,null,1);
        this.appContext = context;
    }

    public DatabaseHelper(Context context) {
        super(context,TABLE_NAME,null,1);
        this.appContext = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + TABLE_NAME + " (" + COL0 + " TEXT PRIMARY KEY , " +
                COL1 +" REAL , " + COL2 + " TEXT , " + COL3 + " INTEGER , "+ COL4 + " TEXT, " + COL5 + " TEXT, " + COL6 +" REAL , " +COL7 +" INTEGER , " +COL8 +" REAL , " +
                COL9 +" REAL , " +COL10 +" TEXT , " +COL11 +" TEXT , " +COL12 +" TEXT , " +COL13 +" TEXT , " +COL14 +" REAL , " +COL15 +" INTEGER , " +COL16 +" TEXT , " +
                COL17 +" REAL , " +COL18 +" INTEGER , " +COL19 +" TEXT , " +COL20 +" REAL , " +COL21 +" INTEGER , " +COL22 +" TEXT , " +COL23 +" TEXT , " +COL24 +" TEXT , " +COL25 +" REAL , " +
                COL26 +" INTEGER , " +COL27 +" TEXT , " + COL28 +" TEXT , " + COL29 +" INTEGER , " +COL30 +" REAL , " +COL31 +" INTEGER , " +
                COL32 +" TEXT , " +COL33 +" TEXT , " + COL34 +" TEXT , " + COL35 + " TEXT , " + COL36 +" TEXT ," + COL37 +" TEXT , " + COL38 +" TEXT )";
        db.execSQL(createTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);


    }

    public boolean addData(Meal meal){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL0 , meal.getMealName() + meal.getLatitude() + meal.getLongitude() + String.valueOf(meal.getusdPrice()));
        contentValues.put(COL1 , meal.getusdPrice());
        contentValues.put(COL2 , meal.getCurrency());
        contentValues.put(COL3 , meal.getTimesVerified());
        contentValues.put(COL4 , meal.getCreatedAt());
        contentValues.put(COL5 , meal.getCreatedBy());
        contentValues.put(COL6 , meal.getMealRatingTotal());
        contentValues.put(COL7 , meal.getNumberOfReviews());
        contentValues.put(COL8 , meal.getLatitude());
        contentValues.put(COL9 , meal.getLongitude());
        contentValues.put(COL10 , meal.getAddress());
        contentValues.put(COL11 , meal.getWorkDayHours());
        contentValues.put(COL12 , meal.getSaturdayHours());
        contentValues.put(COL13 , meal.getSundayHours());
        contentValues.put(COL14 , meal.getSweetTotal());
        contentValues.put(COL15 , meal.getSweetNumber());
        contentValues.put(COL16 , meal.getSweetChangeable().toString());
        contentValues.put(COL17 , meal.getSourTotal());
        contentValues.put(COL18 , meal.getSourNumber());
        contentValues.put(COL19 , meal.getSourChangeable().toString());
        contentValues.put(COL20 , meal.getSaltTotal());
        contentValues.put(COL23 , meal.getSaltNumber());
        contentValues.put(COL22 , meal.getSaltChangeable().toString());
        contentValues.put(COL23 , meal.getBitterTotal());
        contentValues.put(COL24 , meal.getBitterNumber());
        contentValues.put(COL25 , meal.getSpicyTotal());
        contentValues.put(COL26 , meal.getSpicyNumber());
        contentValues.put(COL27 , meal.getSpicyChangeable().toString());
        contentValues.put(COL28 , meal.getUmamiTotal());
        contentValues.put(COL29 , meal.getUmamiNumber());
        contentValues.put(COL30 , meal.getFullnessTotal());
        contentValues.put(COL31 , meal.getFullnessNumber());
        contentValues.put(COL32 , meal.getMealType());
        contentValues.put(COL33 , meal.getPath());
        contentValues.put(COL34 , meal.getGeoHash());
        contentValues.put(COL35 , meal.getMealName());
        contentValues.put(COL36 , meal.getPrimaryTaste());
        contentValues.put(COL37 , meal.getSecondPrimary());
        contentValues.put(COL38 , meal.getMealSubType());

        Log.d("INSERTING DATA LOC", "addData: Adding " + meal.getMealName() + " to " + TABLE_NAME);

        long result = db.insert(TABLE_NAME, null, contentValues);

        //if date as inserted incorrectly it will return -1
        if (result == -1) {
            Log.d("DATA NOT INSERTED:","error");
            return false;
        } else {
            Log.d("DATA INSERTED:","all is well");
            return true;
        }
    }

    public Cursor getDataASC(String hashStart, String hashEnd){
        Log.d("GETTING LOCAL DATA:","getting local data");
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL34 + " <? " + " AND " + COL34 + " >? " + " ORDER BY " + COL0 + " ASC";
        Cursor data = db.rawQuery(query, new String[] {hashEnd,hashStart});

        return data;
    }
}
