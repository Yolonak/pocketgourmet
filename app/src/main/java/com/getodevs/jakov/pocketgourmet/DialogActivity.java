package com.getodevs.jakov.pocketgourmet;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Locale;

import static java.lang.Math.abs;

public class DialogActivity extends AppCompatActivity {
    String name, description, path, longitude, latitude, price, address, activity;
    Bitmap image;
    TextView nameTV, descriptionTV, longitudeTV, latitudeTV, priceTV, addressTV;
    ImageView imageV;
    Button openMapsBtn,rateMealBtn;
    Float x1,x2,y1,y2;
    int position;
    //private DatabaseHelper db;
    private final String TAG = "dialogActivity";
    private Handler mHandler;
    public static ArrayList<Meal> listData = Fragment_list.listData;
    final long ONE_MEGABYTE = 1024 * 1024;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageRef= FirebaseStorage.getInstance().getReference();

    ListView ratingListView;
    static RatingListAdapter adapter;
    public ArrayList<Rating> ratingListData;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    CollectionReference rateRef=db.collection("ratings");


    private void setHandler() {
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                final String imageKey = String.valueOf(path);
                final Bitmap bitmap = MyCache.getInstance().retrieveBitmapFromCache(imageKey);

                /*
                image = BitmapFactory.decodeFile(path);
                adressTV.setText(address);
                imageV.setImageBitmap(image);*/

                addressTV.setText(address);
                longitudeTV.setText(longitude);
                latitudeTV.setText(latitude);
                nameTV.setText(name);
                if (bitmap!=null){
                    imageV.setImageBitmap(bitmap);
                    imageV.setVisibility(View.VISIBLE);
                    Log.d("DIALOG IMAGE:","loading from cache" + path);
                }
                else{
                    storageRef.child(path).getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            Bitmap Image1 = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                            imageV.setImageBitmap(Image1);
                            imageV.setVisibility(View.VISIBLE);
                            MyCache.getInstance().saveBitmapToCahche(String.valueOf(path),Image1);
                            Log.d("DIALOG IMAGE:","loading from storage" + path);


                        }
                    });

                }

            }
        };
    }

    public boolean onTouchEvent(MotionEvent touchEvent) {
        switch (touchEvent.getAction()){
            case MotionEvent.ACTION_DOWN:
                x1 = touchEvent.getX();
                y1 = touchEvent.getY();
            case MotionEvent.ACTION_UP:
                x2 = touchEvent.getX();
                y2 = touchEvent.getY();

                Log.d("dialog activiy", "x1:" + x1 + " x2:" + x2);
                if (x1 < x2 - 200 ) {
                    previousEntry();
                }
                if (x1 > x2 + 200) {
                    nextEntry();
                }
        }
        return false;
    }

    private void nextEntry() {
        int max = listData.size();
        if (position + 1 <= max -1) {
            Intent intent = new Intent(this,DialogActivity.class);
            Meal entry = listData.get(position+1);

            intent.putExtra("name",entry.getMealName());
            //intent.putExtra("description",entry.getDescription());
            intent.putExtra("path",entry.getPath());
            intent.putExtra("price",entry.getusdPrice().toString());
            intent.putExtra("latitude",entry.getLatitude().toString());
            intent.putExtra("longitude",entry.getLongitude().toString());
            intent.putExtra("address",entry.getAddress());
            intent.putExtra("position",position+1);
            intent.putExtra("activity",activity);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            finish();
        }
    }

    private void previousEntry() {
        if (position - 1 >= 0) {
            Intent intent = new Intent(this,DialogActivity.class);
            Meal entry = listData.get(position-1);

            intent.putExtra("name",entry.getMealName());
            //intent.putExtra("description",entry.getDescription());
            intent.putExtra("path",entry.getPath());
            intent.putExtra("price",entry.getusdPrice().toString());
            intent.putExtra("latitude",entry.getLatitude().toString());
            intent.putExtra("longitude",entry.getLongitude().toString());
            intent.putExtra("address",entry.getAddress());
            intent.putExtra("position",position-1);
            intent.putExtra("activity",activity);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            finish();
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);

        ratingListData= new ArrayList<Rating>();
        ratingListView = findViewById(R.id.objectListView);

        Bundle extras = getIntent().getExtras();
        if (extras!=null){

            name = extras.getString("name");
            //description = extras.getString("description");
            path = extras.getString("path");
            longitude = extras.getString("longitude");
            latitude = extras.getString("latitude");
            price = extras.getString("price");
            address = extras.getString("address");
            position = extras.getInt("position");
            activity = extras.getString("activity");
            /*
            switch (activity) {
                case "Search":  listData = SearchResultActivity.listData;
                    break;
            }*/

        }
        Toast.makeText(DialogActivity.this, "lat, lng:" + latitude + "," + longitude, Toast.LENGTH_SHORT).show();

        loadData(name,latitude,longitude,price);



        setHandler();

        Log.d(TAG, "onCreate: " + path + "|" + address);

        //db = new DatabaseHelper(this);

        openMapsBtn = findViewById(R.id.openMapsBtn);
        openMapsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Double longi = Double.parseDouble(longitude);
                Double lati = Double.parseDouble(latitude);
                String uri = String.format(Locale.ENGLISH, "geo:%f,%f", longi, lati);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
        rateMealBtn = findViewById(R.id.rateMealBtn);
        rateMealBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRateDialog();
            }
        });

        nameTV = findViewById(R.id.nameTextView);
        //descriptionTV = findViewById(R.id.descriptionTextView);
        imageV = findViewById(R.id.ImageView);
        longitudeTV = findViewById(R.id.longitudeTextView);
        latitudeTV = findViewById(R.id.latitudeTextView);
        priceTV = findViewById(R.id.priceTextView);
        addressTV = findViewById(R.id.addressTextView);

        nameTV.setText(name);
        //descriptionTV.setText(description);
        //descriptionTV.setMovementMethod(new ScrollingMovementMethod());

        longitudeTV.setText(longitude);
        latitudeTV.setText(latitude);
        priceTV.setText(price);
        Log.d("CHECKING TEXT SET:",priceTV.getText().toString());
        addressTV.setText(address);
        final String imageKey = String.valueOf(path);
        final Bitmap bitmap = MyCache.getInstance().retrieveBitmapFromCache(imageKey);
        if (bitmap!=null){
            imageV.setImageBitmap(bitmap);
            Log.d("DIALOG IMAGE:","loading from cache outside handler" + path);
        }
        else{
            storageRef.child(path).getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap Image1 = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                    imageV.setImageBitmap(Image1);
                    imageV.setVisibility(View.VISIBLE);
                    MyCache.getInstance().saveBitmapToCahche(String.valueOf(path),Image1);
                    Log.d("DIALOG IMAGE:","loading from storage outside handler" + path);


                }
            });

        }


    }

    public void openRateDialog(){
        RateDialog rateDialog=new RateDialog();
        Bundle bundleRate = new Bundle();
        bundleRate.putString("mealID",name+";;"+latitude+";;"+longitude+";;"+price);
        rateDialog.setArguments(bundleRate);
        rateDialog.show(getSupportFragmentManager(), "rate dialog");
    }

    public void loadData(String name, String latitude, String longitude,String price){
        rateRef.whereEqualTo("mealID",name.trim()+";;"+latitude.trim()+";;"+longitude.trim()+";;"+price.trim()).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Rating singleRate=document.toObject(Rating.class);
                                Log.d("LIST OF RATINGS","adding"+singleRate);
                                ratingListData.add(singleRate);
                            }
                            if (ratingListData!=null){
                                adapter = new RatingListAdapter(DialogActivity.this, R.layout.rating_list_view_adapter, ratingListData);
                                ratingListView.setAdapter(adapter);
                                Log.d("LIST OF RATINGS","list is null");
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        Log.d(TAG,listData.toString());
                    }
                });
    }



}
