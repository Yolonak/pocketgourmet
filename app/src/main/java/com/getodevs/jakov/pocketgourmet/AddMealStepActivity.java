package com.getodevs.jakov.pocketgourmet;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.fonfon.geohash.GeoHash;
import com.getodevs.jakov.pocketgourmet.localstorage.DatabaseHelper;
import com.getodevs.jakov.pocketgourmet.stepfragments.ImageStepFragment;
import com.getodevs.jakov.pocketgourmet.stepfragments.LocationStepFragment;
import com.getodevs.jakov.pocketgourmet.stepfragments.TasteStepFragment;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class AddMealStepActivity extends AppCompatActivity implements StepperLayout.StepperListener, TasteStepFragment.TasteData, LocationStepFragment.LocationData, ImageStepFragment.ImageData, ImageStepFragment.PriceData, timeInfoDialog.timeInfoDialogListener {

    private StepperLayout mStepperLayout;
    private String mealName,sweetTasteValue,sourTasteValue,saltTasteValue,bitterTasteValue,spicyTasteValue,umamiTasteValue,mealFullness;
    private Boolean sweetCHB,sourCHB,saltCHB,spicyCHB;
    private String workdayHours,saturdayHours,sundayHours = "unknown";
    private String currency,mealType,mealSubType;
    private Double price;
    private Bitmap bitmap;
    private String latitude,longitude,address;

    //LOCAL STORAGE
    DatabaseHelper dbHelper;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private FirebaseStorage storage=FirebaseStorage.getInstance();

    //duplicates
    //ArrayList<Meal> databaseData;
    //int duplicates;
    CollectionReference mealRef=db.collection("meals");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stepper_layout);
        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        mStepperLayout.setAdapter(new StepperAdapter(getSupportFragmentManager(), this));
        mStepperLayout.setListener(this);

        //local storage
        dbHelper = new DatabaseHelper(this);

        //permissions
        verifyPermissions();
    }


    @Override
    public void onCompleted(View completeButton) {
        if (mealName == null){
            Toast.makeText(this, "mealName missing", Toast.LENGTH_SHORT).show();
        }
        else if (price == null){
            Toast.makeText(this, "price missing" + "." + mealName, Toast.LENGTH_SHORT).show();
        }
        else if (latitude ==null || longitude == null|| address == null){
            Toast.makeText(this, "location info missing", Toast.LENGTH_SHORT).show();
        }
        else if (bitmap == null){
            Toast.makeText(this, "Image missing", Toast.LENGTH_SHORT).show();
        }
        else if (sweetTasteValue.equals("0") && saltTasteValue.equals("0") && sourTasteValue.equals("0") && bitterTasteValue.equals("0") && spicyTasteValue.equals("0") && umamiTasteValue.equals("0")){
            Toast.makeText(this, "No Taste data", Toast.LENGTH_SHORT).show();
        }
        else if (mealFullness.equals("0")){
            Toast.makeText(this, "no Fullness", Toast.LENGTH_SHORT).show();
        }
        else{
            Thread myThread=new Thread(new Runnable() {
                @Override
                public void run() {

                    final String createdAt = Calendar.getInstance().getTime().toString().trim();
                    final String createdBy=user.getEmail().toString().trim();


                    //GeoHash

                    Location location = new Location("geohash");
                    location.setLatitude(Double.parseDouble(latitude));
                    location.setLongitude(Double.parseDouble(longitude));

                    final GeoHash hash = GeoHash.fromLocation(location, 9);
                    Log.d("HASHCODE:",hash.toString());


                    checkForDuplicates(hash.toString(), mealName, price, new OnSuccessListener<Boolean>() {
                        @Override
                        public void onSuccess(Boolean aBoolean) {
                            if (aBoolean){
                                Toast.makeText(AddMealStepActivity.this, "UPLOADING", Toast.LENGTH_SHORT).show();
                                Log.d("addMealBtn","useremail:"+createdBy);

                                ByteArrayOutputStream baos=new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG,60,baos);
                                byte[] data=baos.toByteArray();
                                String imagePath="newMealImages/" + UUID.randomUUID() + ".jpg";
                                StorageReference newMealImagesRef=storage.getReference(imagePath);
                                UploadTask uploadTask = newMealImagesRef.putBytes(data);
                                final Meal meal= new Meal (mealName,price,currency,createdAt,createdBy,Double.parseDouble(latitude),Double.parseDouble(longitude),address,hash.toString(),
                                        workdayHours,saturdayHours,sundayHours,Double.parseDouble(sweetTasteValue),sweetCHB,Double.parseDouble(sourTasteValue), sourCHB,Double.parseDouble(saltTasteValue),
                                        saltCHB,Double.parseDouble(bitterTasteValue),Double.parseDouble(spicyTasteValue),spicyCHB,Double.parseDouble(umamiTasteValue),Double.parseDouble(mealFullness),mealType,imagePath,mealSubType);
                                AddMeal(meal);
                                AddMealLocally(meal);
                            }
                            else{
                                Toast.makeText(AddMealStepActivity.this, "Meal with same name,price and location exists", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    /*
                    Log.d("addMealBtn","useremail:"+createdBy);

                    ByteArrayOutputStream baos=new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG,100,baos);
                    byte[] data=baos.toByteArray();
                    String imagePath="newMealImages/" + UUID.randomUUID() + ".png";
                    StorageReference newMealImagesRef=storage.getReference(imagePath);
                    UploadTask uploadTask = newMealImagesRef.putBytes(data);
                    final Meal meal= new Meal (mealName,price,currency,createdAt,createdBy,Double.parseDouble(latitude),Double.parseDouble(longitude),address,hash.toString(),
                            workdayHours,saturdayHours,sundayHours,Double.parseDouble(sweetTasteValue),sweetCHB,Double.parseDouble(sourTasteValue), sourCHB,Double.parseDouble(saltTasteValue),
                            saltCHB,Double.parseDouble(bitterTasteValue),Double.parseDouble(spicyTasteValue),spicyCHB,Double.parseDouble(mealFullness),mealType,imagePath);
                    AddMeal(meal);
                    AddMealLocally(meal);*/

                }
            });
            myThread.start();
            finish();
        }
        //Toast.makeText(this, "mealName:" + mealName + ";sweetTasteValue:" + sweetTasteValue + "," + sweetCHB.toString(), Toast.LENGTH_SHORT).show();
        //Log.d("IMAGE:",bitmap.toString());

    }

    @Override
    public void onError(VerificationError verificationError) {
        Toast.makeText(this, "onError! -> " + verificationError.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStepSelected(int newStepPosition) {
        Toast.makeText(this, "onStepSelected! -> " + newStepPosition, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReturn() {
        finish();
    }

    @Override
    public void passTasteValues(ArrayList<String> values) {
        Log.d("FRAGMENT COMUNICATION",values.toString());
        mealName = values.get(0);
        sweetTasteValue = values.get(1);
        sourTasteValue = values.get(2);
        saltTasteValue = values.get(3);
        bitterTasteValue = values.get(4);
        spicyTasteValue = values.get(5);
        mealFullness = values.get(6);
        sweetCHB = stringToBool(values.get(7));
        sourCHB = stringToBool(values.get(8));
        saltCHB = stringToBool(values.get(9));
        spicyCHB = stringToBool(values.get(10));
        umamiTasteValue = values.get(11);
        //Log.d("VARIABLES",mealName + "bbbb" + sweetCHB.toString());

    }

    @Override
    public void passImageData(Bitmap image){
        //Log.d("FRAGMENT COMUNICATION:",);
        bitmap = image;

    }

    @Override
    public void passPriceInfo(ArrayList<String> values) {
        if (values.get(0)!=null){
            price = Double.parseDouble(values.get(0));
        }
        else{
            price = null;
        }
        //Log.d("PRICE",price.toString());
        currency = values.get(1);
        mealType = values.get(2);
        mealSubType = values.get(3);

    }

    private boolean stringToBool(String word){
        if (word.equals("True") || word.equals("true")){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void applyTimeInfo(String WDO, String WDC, String STO, String STC, String SNO, String SNC) {
        Log.d("TimePassed",WDO + " " + WDC + " " + STO +" " + STC + " " + SNO + " " + SNC);
        workdayHours = WDO + "-" + WDC;
        saturdayHours = STO + "-" + STC;
        sundayHours = SNO + "-" + SNC;
        Toast.makeText(getApplicationContext(), "AvailableHoursSet:"+ workdayHours + ";" + saturdayHours + ";" + sundayHours , Toast.LENGTH_SHORT).show();

    }

    @Override
    public void passLocationData(ArrayList<String> values) {
        Log.d("FRAGMENT COMUNICATION",values.toString());
        latitude = values.get(0);
        longitude = values.get(1);
        address = values.get(2);
        Log.d("VARIABLES PASSED:",latitude + "," + longitude + "," + address);



    }

    public void AddMeal(Meal meal){
        Log.d("AddingMeal","meal is good to go");
        db.collection("meals")
                .add(meal)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("AddingMeal","DocumentSnapshot added with ID:" + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("AddingMeal", "Error adding document", e);
                    }
                });

    }

    public void AddMealLocally(Meal meal){
        boolean insert = dbHelper.addData(meal);
    }


    public void checkForDuplicates(String geoHash, String name, Double price, final OnSuccessListener<Boolean> onSuccessListener){
        String startHash = geoHash.substring(0,7) + "00";
        String endHash = geoHash.substring(0,7) + "zz";
        //databaseData = new ArrayList<Meal>();
        //duplicates = 0;
        Log.d("DUPLICATE VALUES:",geoHash + "," + name + String.valueOf(price));


        mealRef.whereLessThan("geoHash",endHash).whereGreaterThan("geoHash",startHash).whereEqualTo("mealName",name).whereEqualTo("usdPrice",price)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    private boolean boolDuplicate=false;
                    @Override
                    public void onEvent(QuerySnapshot queryDocumentSnapshots, FirebaseFirestoreException e) {
                        if (!boolDuplicate){
                            boolDuplicate = true;
                            if (e != null) {
                                e.printStackTrace();
                                String message = e.getMessage();
                                onSuccessListener.onSuccess(false);
                                return;
                            }
                            else{
                                List<DocumentSnapshot> snapshotList = queryDocumentSnapshots.getDocuments();
                                if (snapshotList.size() > 0) {
                                    //Field is Exist
                                    onSuccessListener.onSuccess(false);
                                } else {
                                    onSuccessListener.onSuccess(true);
                                }
                            }

                        }
                    }
                });
    }

    private void verifyPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permissions = {android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA};

            if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    permissions[0]) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    permissions[1]) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    permissions[2]) == PackageManager.PERMISSION_GRANTED) {
            } else {
                ActivityCompat.requestPermissions(AddMealStepActivity.this,
                        permissions,
                        1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String[] permissions,  int[] grantResults) {
        //verifyPermissions();
    }

}